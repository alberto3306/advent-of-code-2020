#!/usr/bin/env python3

"""
--- Part Two ---

The Elves in accounting are thankful for your help; one of them even offers you
a starfish coin they had left over from a past vacation. They offer you a second
one if you can find three numbers in your expense report that meet the same
criteria.

Using the above example again, the three entries that sum to 2020 are 979, 366,
and 675. Multiplying them together produces the answer, 241861950.

In your expense report, what is the product of the three entries that sum to
2020?
"""

import os.path


def solve():
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        entries = tuple(map(int, fd.readlines()))
    a, b, c = find_numbers(entries)
    print(a, b, c, a * b * c)


def find_numbers(entries):
    for pos_a, a in enumerate(entries):
        for pos_b, b in enumerate(entries):
            for pos_c, c in enumerate(entries):
                if pos_a != pos_b and pos_b != pos_c and a + b + c == 2020:
                    return a, b, c


if __name__ == '__main__':
    solve()
