#!/usr/bin/env python3

"""
--- Day 2: Password Philosophy ---

Your flight departs in a few days from the coastal airport; the easiest way down
to the coast from here is via toboggan.

The shopkeeper at the North Pole Toboggan Rental Shop is having a bad day.
"Something's wrong with our computers; we can't log in!" You ask if you can take
a look.

Their password database seems to be a little corrupted: some of the passwords
wouldn't have been allowed by the Official Toboggan Corporate Policy that was in
effect when they were chosen.

To try to debug the problem, they have created a list (your puzzle input) of
passwords (according to the corrupted database) and the corporate policy when
that password was set.

For example, suppose you have the following list:

1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc

Each line gives the password policy and then the password. The password policy
indicates the lowest and highest number of times a given letter must appear for
the password to be valid. For example, 1-3 a means that the password must
contain a at least 1 time and at most 3 times.

In the above example, 2 passwords are valid. The middle password, cdefg, is not;
it contains no instances of b, but needs at least 1. The first and third
passwords are valid: they contain one a or nine c, both within the limits of
their respective policies.

How many passwords are valid according to their policies?

"""

import os.path
import re
from collections import Counter

LINE_PATTERN = r'^(\d+)\-(\d+) (\w): (\w+)$'


def solve():
    lines = load_input_file()
    entries = map(parse_line, lines)
    print(count_valid_passwords(entries))


def load_input_file():
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return filter(None, fd.readlines())


def parse_line(line):
    match = re.match(LINE_PATTERN, line)
    min_count, max_count, char, password = match.groups()
    return (
        int(min_count),
        int(max_count),
        char,
        password,
    )


def count_valid_passwords(entries):
    result = 0
    for entry in entries:
        result += validate_entry(*entry)
    return result


def validate_entry(min_count, max_count, char, password):
    char_counter = Counter(password)
    return min_count <= char_counter[char] <= max_count


if __name__ == '__main__':
    solve()
