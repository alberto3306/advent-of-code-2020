#!/usr/bin/env python3

"""
--- Part Two ---

While it appears you validated the passwords correctly, they don't seem to be
what the Official Toboggan Corporate Authentication System is expecting.

The shopkeeper suddenly realizes that he just accidentally explained the
password policy rules from his old job at the sled rental place down the street!
The Official Toboggan Corporate Policy actually works a little differently.

Each policy actually describes two positions in the password, where 1 means the
first character, 2 means the second character, and so on. (Be careful; Toboggan
Corporate Policies have no concept of "index zero"!) Exactly one of these
positions must contain the given letter. Other occurrences of the letter are
irrelevant for the purposes of policy enforcement.

Given the same example list from above:

    1-3 a: abcde is valid: position 1 contains a and position 3 does not.
    1-3 b: cdefg is invalid: neither position 1 nor position 3 contains b.
    2-9 c: ccccccccc is invalid: both position 2 and position 9 contain c.

How many passwords are valid according to the new interpretation of the policies?

"""

import os.path
import re

LINE_PATTERN = r'^(\d+)\-(\d+) (\w): (\w+)$'


def solve():
    lines = load_input_file()
    entries = map(parse_line, lines)
    print(count_valid_passwords(entries))


def load_input_file():
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return filter(None, fd.readlines())


def parse_line(line):
    match = re.match(LINE_PATTERN, line)
    pos_one, pos_two, char, password = match.groups()
    return (
        int(pos_one),
        int(pos_two),
        char,
        password,
    )


def count_valid_passwords(entries):
    result = 0
    for entry in entries:
        result += validate_entry(*entry)
    return result


def validate_entry(pos_one, pos_two, char, password):
    check_one = password[pos_one - 1] == char
    check_two = password[pos_two - 1] == char
    return check_one ^ check_two


if __name__ == '__main__':
    solve()
