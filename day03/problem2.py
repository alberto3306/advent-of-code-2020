#!/usr/bin/env python3

"""
--- Part Two ---

Time to check the rest of the slopes - you need to minimize the probability of a
sudden arboreal stop, after all.

Determine the number of trees you would encounter if, for each of the following
slopes, you start at the top-left corner and traverse the map all the way to the
bottom:

    Right 1, down 1.
    Right 3, down 1. (This is the slope you already checked.)
    Right 5, down 1.
    Right 7, down 1.
    Right 1, down 2.

In the above example, these slopes would find 2, 7, 3, 4, and 2 tree(s)
respectively; multiplied together, these produce the answer 336.

What do you get if you multiply together the number of trees encountered on each
of the listed slopes?

"""

import os.path
import unittest
from functools import reduce
from operator import mul

TREE = '#'

SLOPES = (
    (1, 1),
    (3, 1),
    (5, 1),
    (7, 1),
    (1, 2),
)


def solve():
    lines = load_input_file()
    trees_found = (
        count_trees(lines, x, y)
        for x, y in SLOPES
    )
    result = reduce(mul, trees_found)
    print(result)


def load_input_file():
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return tuple(filter(None, map(str.rstrip, fd.readlines())))


def count_trees(lines, x, y):
    result = 0
    pos_x = 0
    for line in lines[::y]:
        is_tree = line[pos_x] == TREE
        result += is_tree
        pos_x += x
        pos_x %= len(line)
    return result


if __name__ == '__main__':
    solve()


class Tests(unittest.TestCase):

    def test_count_trees(self):

        # Having a map of trees
        forest = (
            '..##.......',
            '#...#...#..',
            '.#....#..#.',
            '..#.#...#.#',
            '.#...##..#.',
            '..#.##.....',
            '.#.#.#....#',
            '.#........#',
            '#.##...#...',
            '#...##....#',
            '.#..#...#.#',
        )

        # And some directions to follow
        cases = (
            (1, 1, 2),
            (3, 1, 7),
            (5, 1, 3),
            (7, 1, 4),
            (1, 2, 2),
        )

        # When counting the trees
        for x, y, expected in cases:
            result = count_trees(forest, x, y)

        # The result should be the one expected
            self.assertEqual(result, expected)
