#!/usr/bin/env python3

"""
--- Day 4: Passport Processing ---

You arrive at the airport only to realize that you grabbed your North Pole
Credentials instead of your passport. While these documents are extremely
similar, North Pole Credentials aren't issued by a country and therefore aren't
actually valid documentation for travel in most of the world.

It seems like you're not the only one having problems, though; a very long line
has formed for the automatic passport scanners, and the delay could upset your
travel itinerary.

Due to some questionable network security, you realize you might be able to
solve both of these problems at the same time.

The automatic passport scanners are slow because they're having trouble
detecting which passports have all required fields. The expected fields are as
follows:

    byr (Birth Year)
    iyr (Issue Year)
    eyr (Expiration Year)
    hgt (Height)
    hcl (Hair Color)
    ecl (Eye Color)
    pid (Passport ID)
    cid (Country ID)

Passport data is validated in batch files (your puzzle input). Each passport is
represented as a sequence of key:value pairs separated by spaces or newlines.
Passports are separated by blank lines.

Here is an example batch file containing four passports:

ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884 hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648 iyr:2011 ecl:brn hgt:59in

The first passport is valid - all eight fields are present. The second passport
is invalid - it is missing hgt (the Height field).

The third passport is interesting; the only missing field is cid, so it looks
like data from North Pole Credentials, not a passport at all! Surely, nobody
would mind if you made the system temporarily ignore missing cid fields. Treat
this "passport" as valid.

The fourth passport is missing two fields, cid and byr. Missing cid is fine, but
missing any other field is not, so this passport is invalid.

According to the above rules, your improved system would report 2 valid
passports.

Count the number of valid passports - those that have all required fields. Treat
cid as optional. In your batch file, how many passports are valid?

"""

import re
import os.path
import unittest

FIELD_PATTERN = r'(\S+):(\S+)'
PASSPORT_PATTERN = r'(%s\s)+' % FIELD_PATTERN

REQUIRED_FIELDS = (
    'byr',  # Birth Year
    'iyr',  # Issue Year
    'eyr',  # Expiration Year
    'hgt',  # Height
    'hcl',  # Hair Color
    'ecl',  # Eye Color
    'pid',  # Passport ID
    # 'cid', # Country ID
)


def solve():
    data = load_input_file()
    print(count_valid_passports(data))


def load_input_file():
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return fd.read()


def count_valid_passports(lines):
    passports = extract_passports(lines)
    return sum(map(validate_passport, passports))


def extract_passports(data):
    matches = re.finditer(PASSPORT_PATTERN, data)
    return (
        dict(re.findall(FIELD_PATTERN, match.group()))
        for match in matches
    )


def validate_passport(passport):
    return set(REQUIRED_FIELDS) <= set(passport.keys())


if __name__ == '__main__':
    solve()


class Tests(unittest.TestCase):

    def test_extract_passports(self):

        # Having data for some passports
        data = (
            'abc:123 def:asdfg ghi:aaaa\n'
            'jkl:456 mno:zxxcv pqr:bbbb\n'
            '\n'
            'omg:aaa wtf:zzz bbq:111\n'
        )

        # When trying to extract the passports
        result = extract_passports(data)
        result = tuple(result)

        # The number of passports extracted is correct
        self.assertEqual(len(result), 2)

        # The passport fields have been exctracted
        self.assertDictEqual(result[0], {
            'abc': '123', 'def': 'asdfg', 'ghi': 'aaaa',
            'jkl': '456', 'mno': 'zxxcv', 'pqr': 'bbbb',
        })
        self.assertDictEqual(result[1], {
            'omg': 'aaa', 'wtf': 'zzz', 'bbq': '111',
        })

    def test_validate_passport(self):

        # Have some passports
        cases = (
            (True, {'ecl': 'gry', 'pid': '860033327', 'eyr': '2020', 'hcl': '#fffffd', 'byr': '1937', 'iyr': '2017', 'cid': '147', 'hgt': '183cm'}),
            (False, {'iyr': '2013', 'ecl': 'amb', 'cid': '350', 'eyr': '2023', 'pid': '028048884', 'hcl': '#cfa07d', 'byr': '1929'}),
            (True, {'hcl': '#ae17e1', 'iyr': '2013', 'eyr': '2024', 'ecl': 'brn', 'pid': '760753108', 'byr': '1931', 'hgt': '179cm'}),
            (False, {'hcl': '#cfa07d', 'eyr': '2025', 'pid': '166559648', 'iyr': '2011', 'ecl': 'brn', 'hgt': '59in'}),
        )

        # When validating the required fields presence
        for expected, passport in cases:
            result = validate_passport(passport)

            # The result should be the expected
            self.assertEqual(result, expected)
