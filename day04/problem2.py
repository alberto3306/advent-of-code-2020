#!/usr/bin/env python3

"""
--- Part Two ---

The line is moving more quickly now, but you overhear airport security talking
about how passports with invalid data are getting through. Better add some data
validation, quick!

You can continue to ignore the cid field, but each other field has strict rules
about what values are valid for automatic validation:

    byr (Birth Year) - four digits; at least 1920 and at most 2002.
    iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    hgt (Height) - a number followed by either cm or in:
        If cm, the number must be at least 150 and at most 193.
        If in, the number must be at least 59 and at most 76.
    hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    pid (Passport ID) - a nine-digit number, including leading zeroes.
    cid (Country ID) - ignored, missing or not.

Your job is to count the passports where all required fields are both present
and valid according to the above rules. Here are some example values:

byr valid:   2002 
byr invalid: 2003

hgt valid:   60in 
hgt valid:   190cm 
hgt invalid: 190in 
hgt invalid: 190

hcl valid:   #123abc 
hcl invalid: #123abz 
hcl invalid: 123abc

ecl valid:   brn 
ecl invalid: wat

pid valid:   000000001 
pid invalid: 0123456789

Here are some invalid passports:

 * eyr:1972 cid:100 hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926
 * iyr:2019 hcl:#602927 eyr:1967 hgt:170cm ecl:grn pid:012533040 byr:1946
 * hcl:dab227 iyr:2012 ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277
 * hgt:59cm ecl:zzz eyr:2038 hcl:74454a iyr:2023 pid:3556412378 byr:2007

Here are some valid passports:

 * pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980 hcl:#623a2f
 * eyr:2029 ecl:blu cid:129 byr:1989 iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm
 * hcl:#888785 hgt:164cm byr:2001 iyr:2015 cid:88 pid:545766238 ecl:hzl eyr:2022
 * iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719

Count the number of valid passports - those that have all required fields and
valid values. Continue to treat cid as optional. In your batch file, how many
passports are valid?

"""

import re
import os.path
import unittest

FIELD_PATTERN = r'(\S+):(\S+)'
PASSPORT_PATTERN = r'(%s\s)+' % FIELD_PATTERN

REQUIRED_FIELDS = (
    'byr',  # Birth Year
    'iyr',  # Issue Year
    'eyr',  # Expiration Year
    'hgt',  # Height
    'hcl',  # Hair Color
    'ecl',  # Eye Color
    'pid',  # Passport ID
    # 'cid', # Country ID
)


def solve():
    data = load_input_file()
    print(count_valid_passports(data))


def load_input_file():
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return fd.read()


def count_valid_passports(lines):
    passports = extract_passports(lines)
    return sum(map(validate_passport, passports))


def extract_passports(data):
    matches = re.finditer(PASSPORT_PATTERN, data)
    return (
        dict(re.findall(FIELD_PATTERN, match.group()))
        for match in matches
    )


def validate_passport(passport):
    return (
        set(REQUIRED_FIELDS) <= set(passport.keys())
        and all(map(validate_field, passport.keys(), passport.values()))
    )


def validate_field(field, value):
    validator = FIELD_VALIDATORS.get(field, bool)
    return bool(validator(value))


def validate_hgt(value):
    match = re.fullmatch('(\d+)(cm|in)', value)
    if not match:
        return False
    num, unit = match.groups()
    num_min, num_max = HGT_LIMITS[unit]
    return num_min <= int(num) <= num_max


FIELD_VALIDATORS = {
    'byr': lambda x: 1920 <= int(x) <= 2002,  # (Birth Year) - four digits; at least 1920 and at most 2002.
    'iyr': lambda x: 2010 <= int(x) <= 2020,  # (Issue Year) - four digits; at least 2010 and at most 2020.
    'eyr': lambda x: 2020 <= int(x) <= 2030,  # (Expiration Year) - four digits; at least 2020 and at most 2030.
    'hgt': validate_hgt,
    # (Height) - a number followed by either cm or in:
    #     If cm, the number must be at least 150 and at most 193.
    #     If in, the number must be at least 59 and at most 76.
    'hcl': lambda x: re.fullmatch(r'#[0-9a-f]{6}', x),  # (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    'ecl': lambda x: x in ('amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'),  # (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    'pid': lambda x: re.fullmatch('\d{9}', x),  # (Passport ID) - a nine-digit number, including leading zeroes.
    # 'cid': lambda x: True,  # (Country ID) - ignored, missing or not.
}

HGT_LIMITS = {
    'cm': (150, 193),
    'in': (59, 76),
}


if __name__ == '__main__':
    solve()


class Tests(unittest.TestCase):

    def test_extract_passports(self):

        # Having data for some passports
        data = (
            'abc:123 def:asdfg ghi:aaaa\n'
            'jkl:456 mno:zxxcv pqr:bbbb\n'
            '\n'
            'omg:aaa wtf:zzz bbq:111\n'
        )

        # When trying to extract the passports
        result = extract_passports(data)
        result = tuple(result)

        # The number of passports extracted is correct
        self.assertEqual(len(result), 2)

        # The passport fields have been exctracted
        self.assertDictEqual(result[0], {
            'abc': '123', 'def': 'asdfg', 'ghi': 'aaaa',
            'jkl': '456', 'mno': 'zxxcv', 'pqr': 'bbbb',
        })
        self.assertDictEqual(result[1], {
            'omg': 'aaa', 'wtf': 'zzz', 'bbq': '111',
        })

    def test_validate_field(self):

        # Having some field and values
        cases = (
            ('byr', '2002', True),
            ('byr', '2003', False),
            ('hgt', '60in', True),
            ('hgt', '190cm', True),
            ('hgt', '190in', False),
            ('hgt', '190', False),
            ('hcl', '#123abc', True),
            ('hcl', '#123abz', False),
            ('hcl', '123abc', False),
            ('ecl', 'brn', True),
            ('ecl', 'wat', False),
            ('pid', '000000001', True),
            ('pid', '0123456789', False),
        )

        for field, value, expected in cases:

            # When checking their validity
            result = validate_field(field, value)

            # The result should be the expected
            self.assertEqual(result, expected, f'{field}: {value} should be {expected}')

    def test_validate_passport(self):

        # Have some passports
        cases = (
            (False, {'eyr': '1972', 'cid': '100', 'hcl': '#18171d', 'ecl': 'amb', 'hgt': '170', 'pid': '186cm', 'iyr': '2018', 'byr': '1926'}),
            (False, {'iyr': '2019', 'hcl': '#602927', 'eyr': '1967', 'hgt': '170cm', 'ecl': 'grn', 'pid': '012533040', 'byr': '1946'}),
            (False, {'hcl': 'dab227', 'iyr': '2012', 'ecl': 'brn', 'hgt': '182cm', 'pid': '021572410', 'eyr': '2020', 'byr': '1992', 'cid': '277'}),
            (False, {'hgt': '59cm', 'ecl': 'zzz', 'eyr': '2038', 'hcl': '74454a', 'iyr': '2023', 'pid': '3556412378', 'byr': '2007'}),
            (True, {'pid': '087499704', 'hgt': '74in', 'ecl': 'grn', 'iyr': '2012', 'eyr': '2030', 'byr': '1980', 'hcl': '#623a2f'}),
            (True, {'eyr': '2029', 'ecl': 'blu', 'cid': '129', 'byr': '1989', 'iyr': '2014', 'pid': '896056539', 'hcl': '#a97842', 'hgt': '165cm'}),
            (True, {'hcl': '#888785', 'hgt': '164cm', 'byr': '2001', 'iyr': '2015', 'cid': '88', 'pid': '545766238', 'ecl': 'hzl', 'eyr': '2022'}),
            (True, {'iyr': '2010', 'hgt': '158cm', 'hcl': '#b6652a', 'ecl': 'blu', 'byr': '1944', 'eyr': '2021', 'pid': '093154719'}),
        )

        # When validating the password fields
        for expected, passport in cases:
            result = validate_passport(passport)

            # The result should be the expected
            self.assertEqual(result, expected, f'{passport} should be {expected}')
