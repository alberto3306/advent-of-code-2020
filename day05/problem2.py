#!/usr/bin/env python3

"""
--- Part Two ---

Ding! The "fasten seat belt" signs have turned on. Time to find your seat.

It's a completely full flight, so your seat should be the only missing boarding
pass in your list. However, there's a catch: some of the seats at the very front
and back of the plane don't exist on this aircraft, so they'll be missing from
your list as well.

Your seat wasn't at the very front or back, though; the seats with IDs +1 and -1
from yours will be in your list.

What is the ID of your seat?

"""

import os.path
import unittest
from typing import Iterable


def solve():
    boarding_passes = load_input_file()
    print(find_empty_seat(boarding_passes))


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def find_empty_seat(boarding_passes: Iterable[str]) -> int:
    ids = map(decode_boarding_pass_id, boarding_passes)
    ids = iter(sorted(ids))
    prev_seat = next(ids)
    while True:
        expected = prev_seat + 1
        next_seat = next(ids)
        if next_seat != expected:
            return expected
        prev_seat = next_seat


CODE_TRANS = str.maketrans('FBLR', '0101')


def decode_boarding_pass_id(code: str) -> int:
    """
    Extract the ID from a boarding pass code.
    """
    bincode = code.translate(CODE_TRANS)
    return int(bincode, 2)


class Tests(unittest.TestCase):

    def test_decode_boarding_pass_id(self):

        # Having some encoded boarding passes
        cases = (
            ('BFFFBBFRRR', 567),
            ('FFFBBBFRRR', 119),
            ('BBFFBBFRLL', 820),
        )

        for code, expected in cases:

            # When decoding them
            result = decode_boarding_pass_id(code)

            # The returned IDs should be the expected
            self.assertEqual(result, expected, f'Wrong id decoded from {code}.')


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    solve()
