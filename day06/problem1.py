#!/usr/bin/env python3

"""
--- Day 6: Custom Customs ---

As your flight approaches the regional airport where you'll switch to a much
larger plane, customs declaration forms are distributed to the passengers.

The form asks a series of 26 yes-or-no questions marked a through z. All you
need to do is identify the questions for which anyone in your group answers
"yes". Since your group is just you, this doesn't take very long.

However, the person sitting next to you seems to be experiencing a language
barrier and asks if you can help. For each of the people in their group, you
write down the questions for which they answer "yes", one per line. For example:

abcx
abcy
abcz

In this group, there are 6 questions to which anyone answered "yes": a, b, c, x,
y, and z. (Duplicate answers to the same question don't count extra; each
question counts at most once.)

Another group asks for your help, then another, and eventually you've collected
answers from every group on the plane (your puzzle input). Each group's answers
are separated by a blank line, and within each group, each person's answers are
on a single line. For example:

abc

a
b
c

ab
ac

a
a
a
a

b

This list represents answers from five groups:

    The first group contains one person who answered "yes" to 3 questions: a, b, and c.
    The second group contains three people; combined, they answered "yes" to 3 questions: a, b, and c.
    The third group contains two people; combined, they answered "yes" to 3 questions: a, b, and c.
    The fourth group contains four people; combined, they answered "yes" to only 1 question, a.
    The last group contains one person who answered "yes" to only 1 question, b.

In this example, the sum of these counts is 3 + 3 + 3 + 1 + 1 = 11.

For each group, count the number of questions to which anyone answered "yes".
What is the sum of those counts?

"""

import os.path
import unittest
from typing import Iterable


def solve():
    lines = load_input_file()
    print(count_unique_answers(lines))


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def count_unique_answers(lines: Iterable[str]) -> int:
    group_unique_answers = count_group_unique_answers(lines)
    return sum(group_unique_answers)


def count_group_unique_answers(lines: Iterable[str]) -> Iterable[int]:
    group_answers = set()
    for line in lines:
        answers = set(line)
        if not answers:
            yield len(group_answers)
            group_answers.clear()
        else:
            group_answers |= answers
    yield len(group_answers)


class Tests(unittest.TestCase):

    lines = (
        'abc',
        '',
        'a',
        'b',
        'c',
        '',
        'ab',
        'ac',
        '',
        'a',
        'a',
        'a',
        'a',
        '',
        'b',
    )

    def test_count_unique_answers(self):
        result = count_unique_answers(self.lines)
        self.assertEqual(result, 11)

    def test_count_group_unique_answers(self):
        result = count_group_unique_answers(self.lines)
        expected = [3, 3, 3, 1, 1]
        self.assertSequenceEqual(list(result), expected)


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    solve()
