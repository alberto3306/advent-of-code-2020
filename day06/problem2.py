#!/usr/bin/env python3

"""
--- Part Two ---

As you finish the last group's customs declaration, you notice that you misread
one word in the instructions:

You don't need to identify the questions to which anyone answered "yes"; you
need to identify the questions to which everyone answered "yes"!

Using the same example as above:

abc

a
b
c

ab
ac

a
a
a
a

b

This list represents answers from five groups:

    In the first group, everyone (all 1 person) answered "yes" to 3 questions: a, b, and c.
    In the second group, there is no question to which everyone answered "yes".
    In the third group, everyone answered yes to only 1 question, a. Since some people did not answer "yes" to b or c, they don't count.
    In the fourth group, everyone answered yes to only 1 question, a.
    In the fifth group, everyone (all 1 person) answered "yes" to 1 question, b.

In this example, the sum of these counts is 3 + 0 + 1 + 1 + 1 = 6.

For each group, count the number of questions to which everyone answered "yes".
What is the sum of those counts?

"""

import os.path
import unittest
from string import ascii_lowercase
from typing import Iterable


def solve():
    lines = load_input_file()
    print(count_common_answers(lines))


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def count_common_answers(lines: Iterable[str]) -> int:
    group_common_answers = count_group_common_answers(lines)
    return sum(group_common_answers)


def count_group_common_answers(lines: Iterable[str]) -> Iterable[int]:
    group_answers = set(ascii_lowercase)
    for line in lines:
        if not line:
            yield len(group_answers)
            group_answers = set(ascii_lowercase)
        else:
            group_answers &= set(line)
    yield len(group_answers)


class Tests(unittest.TestCase):

    lines = (
        'abc',
        '',
        'a',
        'b',
        'c',
        '',
        'ab',
        'ac',
        '',
        'a',
        'a',
        'a',
        'a',
        '',
        'b',
    )

    def test_count_common_answers(self):
        result = count_common_answers(self.lines)
        self.assertEqual(result, 6)

    def test_count_group_common_answers(self):
        result = count_group_common_answers(self.lines)
        expected = [3, 0, 1, 1, 1]
        self.assertSequenceEqual(list(result), expected)


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    solve()
