
class Bag:

    def __init__(self, color: str) -> None:
        self.color = color
        self.parents = {}
        self.children = {}

    def __str__(self):
        return self.color

    def add_parent(self, parent: 'Bag', quantity: int) -> None:
        self.parents[parent] = quantity

    def add_child(self, child: 'Bag', quantity: int) -> None:
        self.children[child] = quantity
