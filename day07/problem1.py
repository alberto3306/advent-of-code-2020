#!/usr/bin/env python3

"""
--- Day 7: Handy Haversacks ---

You land at the regional airport in time for your next flight. In fact, it looks
like you'll even have time to grab some food: all flights are currently delayed
due to issues in luggage processing.

Due to recent aviation regulations, many rules (your puzzle input) are being
enforced about bags and their contents; bags must be color-coded and must
contain specific quantities of other color-coded bags. Apparently, nobody
responsible for these regulations considered how long they would take to
enforce!

For example, consider the following rules:

    * light red bags contain 1 bright white bag, 2 muted yellow bags.
    * dark orange bags contain 3 bright white bags, 4 muted yellow bags.
    * bright white bags contain 1 shiny gold bag.
    * muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
    * shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
    * dark olive bags contain 3 faded blue bags, 4 dotted black bags.
    * vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
    * faded blue bags contain no other bags.
    * dotted black bags contain no other bags.

These rules specify the required contents for 9 bag types. In this example,
every faded blue bag is empty, every vibrant plum bag contains 11 bags (5 faded
blue and 6 dotted black), and so on.

You have a shiny gold bag. If you wanted to carry it in at least one other bag,
how many different bag colors would be valid for the outermost bag? (In other
words: how many colors can, eventually, contain at least one shiny gold bag?)

In the above rules, the following options would be available to you:

    * A bright white bag, which can hold your shiny gold bag directly.
    * A muted yellow bag, which can hold your shiny gold bag directly,
      plus some other bags.
    * A dark orange bag, which can hold bright white and muted yellow bags,
      either of which could then hold your shiny gold bag.
    * A light red bag, which can hold bright white and muted yellow bags,
      either of which could then hold your shiny gold bag.

So, in this example, the number of bag colors that can eventually contain at
least one shiny gold bag is 4.

How many bag colors can eventually contain at least one shiny gold bag?
"""

import os.path
import re
import unittest
from typing import Iterable

from tree import Tree


def solve():
    lines = load_input_file()
    tree = build_rule_tree(lines)
    parents = tree.find_all_parents('shiny gold')
    print(len(parents))


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def build_rule_tree(lines):
    decoded = map(decode_line, lines)
    tree = Tree()
    for parent, contents in decoded:
        tree.add_node(parent)
        for quantity, child in contents:
            tree.add_parent(parent, child, quantity)
    return tree


LINE_EXPR = r'^(\w+ \w+) bags contain (.+)'
CHILD_EXPR = r'(\d+) (\w+ \w+) bags?'


def decode_line(line: str):
    match = re.match(LINE_EXPR, line)
    color, ch = match.groups()
    children = tuple(
        (int(quantity), color)
        for quantity, color in re.findall(CHILD_EXPR, ch)
    )
    return color, children


class Tests(unittest.TestCase):
    # pylint: disable=protected-access

    lines = (
        'light red bags contain 1 bright white bag, 2 muted yellow bags.',
        'dark orange bags contain 3 bright white bags, 4 muted yellow bags.',
        'bright white bags contain 1 shiny gold bag.',
        'muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.',
        'shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.',
        'dark olive bags contain 3 faded blue bags, 4 dotted black bags.',
        'vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.',
        'faded blue bags contain no other bags.',
        'dotted black bags contain no other bags.',
    )

    def test_build_tree(self):
        tree = build_rule_tree(self.lines)
        bag = tree._get_or_create('shiny gold')
        self.assertEqual(len(bag.parents), 2)

    def test_find_all_parents(self):
        tree = build_rule_tree(self.lines)
        parents = tree.find_all_parents('shiny gold')
        self.assertEqual(len(parents), 4)

    def test_decode_line(self):

        cases = (
            ('light red', ((1, 'bright white'), (2, 'muted yellow'))),
            ('dark orange', ((3, 'bright white'), (4, 'muted yellow'))),
            ('bright white', ((1, 'shiny gold'),)),
            ('muted yellow', ((2, 'shiny gold'), (9, 'faded blue'))),
            ('shiny gold', ((1, 'dark olive'), (2, 'vibrant plum'))),
            ('dark olive', ((3, 'faded blue'), (4, 'dotted black'))),
            ('vibrant plum', ((5, 'faded blue'), (6, 'dotted black'))),
            ('faded blue', ()),
            ('dotted black', ()),
        )

        for line, expected in zip(self.lines, cases):
            result = decode_line(line)
            self.assertSequenceEqual(result, expected)


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    solve()
