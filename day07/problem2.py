#!/usr/bin/env python3

"""
--- Part Two ---

It's getting pretty expensive to fly these days - not because of ticket prices,
but because of the ridiculous number of bags you need to buy!

Consider again your shiny gold bag and the rules from the above example:

    faded blue bags contain 0 other bags.
    dotted black bags contain 0 other bags.
    vibrant plum bags contain 11 other bags: 5 faded blue bags and 6 dotted black bags.
    dark olive bags contain 7 other bags: 3 faded blue bags and 4 dotted black bags.

So, a single shiny gold bag must contain 1 dark olive bag (and the 7 bags within
it) plus 2 vibrant plum bags (and the 11 bags within each of those): 1 + 1*7 + 2
+ 2*11 = 32 bags!

Of course, the actual rules have a small chance of going several levels deeper
than this example; be sure to count all of the bags, even if the nesting becomes
topologically impractical!

Here's another example:

shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.

In this example, a single shiny gold bag must contain 126 other bags.

How many individual bags are required inside your single shiny gold bag?
"""

import os.path
import re
import unittest
from typing import Iterable

from tree import Tree


def solve():
    lines = load_input_file()
    tree = build_rule_tree(lines)
    print(tree.count_required_children('shiny gold'))


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def build_rule_tree(lines):
    decoded = map(decode_line, lines)
    tree = Tree()
    for parent, contents in decoded:
        tree.add_node(parent)
        for quantity, child in contents:
            tree.add_parent(parent, child, quantity)
    return tree


LINE_EXPR = r'^(\w+ \w+) bags contain (.+)'
CHILD_EXPR = r'(\d+) (\w+ \w+) bags?'


def decode_line(line: str):
    match = re.match(LINE_EXPR, line)
    color, ch = match.groups()
    children = tuple(
        (int(quantity), color)
        for quantity, color in re.findall(CHILD_EXPR, ch)
    )
    return color, children


class Tests(unittest.TestCase):
    # pylint: disable=protected-access

    lines = (
        'light red bags contain 1 bright white bag, 2 muted yellow bags.',
        'dark orange bags contain 3 bright white bags, 4 muted yellow bags.',
        'bright white bags contain 1 shiny gold bag.',
        'muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.',
        'shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.',
        'dark olive bags contain 3 faded blue bags, 4 dotted black bags.',
        'vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.',
        'faded blue bags contain no other bags.',
        'dotted black bags contain no other bags.',
    )

    def test_build_tree(self):
        tree = build_rule_tree(self.lines)
        bag = tree._get_or_create('shiny gold')
        self.assertEqual(len(bag.parents), 2)

    def test_count_required_bags(self):
        tree = build_rule_tree(self.lines)
        result = tree.count_required_children('shiny gold')
        self.assertEqual(result, 32)

    def test_decode_line(self):

        cases = (
            ('light red', ((1, 'bright white'), (2, 'muted yellow'))),
            ('dark orange', ((3, 'bright white'), (4, 'muted yellow'))),
            ('bright white', ((1, 'shiny gold'),)),
            ('muted yellow', ((2, 'shiny gold'), (9, 'faded blue'))),
            ('shiny gold', ((1, 'dark olive'), (2, 'vibrant plum'))),
            ('dark olive', ((3, 'faded blue'), (4, 'dotted black'))),
            ('vibrant plum', ((5, 'faded blue'), (6, 'dotted black'))),
            ('faded blue', ()),
            ('dotted black', ()),
        )

        for line, expected in zip(self.lines, cases):
            result = decode_line(line)
            self.assertSequenceEqual(result, expected)


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    solve()
