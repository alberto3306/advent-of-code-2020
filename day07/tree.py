from typing import Iterable, Set

from bag import Bag


class Tree:

    def __init__(self) -> None:
        self.colors = {}

    def add_node(self, color: str) -> None:
        self._get_or_create(color)

    def add_parent(self, parent_color: str, child_color: str, quantity: int) -> None:
        parent = self._get_or_create(parent_color)
        child = self._get_or_create(child_color)
        child.add_parent(parent, quantity)
        parent.add_child(child, quantity)

    def find_all_parents(self, color: str) -> Set[str]:
        bag = self._get_or_create(color)
        return set(bag.color for bag in self._yield_parents(bag))

    def count_required_children(self, color: str) -> int:
        bag = self._get_or_create(color)
        return sum(self._yield_required_children(bag))

    def _get_or_create(self, color: str) -> Bag:
        try:
            result = self.colors[color]
        except KeyError:
            result = Bag(color)
            self.colors[color] = result
        return result

    def _yield_parents(self, bag: Bag, done=None) -> Iterable[Bag]:
        done = done or {bag}
        for parent in bag.parents:
            if parent not in done:
                yield parent
                done.add(parent)
                yield from self._yield_parents(parent, done)

    def _yield_required_children(self, bag: Bag):
        for child, quantity in bag.children.items():
            yield quantity
            for c in self._yield_required_children(child):
                yield quantity * c
