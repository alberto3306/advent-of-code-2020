import re
from typing import Any, Callable, Iterable, Sequence, Union


class State:

    cursor = 0
    accumulator = 0
    success = None


class Bootloader:

    LINE_EXPR = r'(\w{3}) ([\+\-]\d+)'

    def __init__(self, code: Iterable[str]) -> None:
        self._code = self._load_code(code)

    def run(self) -> int:
        """
        Run the loaded code until an infinite loop is detected.
        """
        state = self._run()
        return state.accumulator

    def run_with_fix(self):
        """
        Run the code while trying to fix it.
        """

        for line, fix in self._find_possible_fixes():
            state = self._run(line, fix)
            if state.success:
                return state.accumulator

    def _load_code(self, code: Iterable[str]) -> Sequence[Union[Callable, int]]:
        return tuple(map(self._decode_line, code))

    def _find_possible_fixes(self) -> Iterable[Union[int, Callable]]:
        # pylint: disable=comparison-with-callable
        yield -1, None
        for line, (instruction, _) in enumerate(self._code):
            if instruction == self._instruction_jmp:
                yield line, self._instruction_nop
            if instruction == self._instruction_nop:
                yield line, self._instruction_jmp

    def _decode_line(self, line: str) -> Union[Callable, int]:
        match = re.match(self.LINE_EXPR, line)
        instruction_name, argument = match.groups()
        instruction = getattr(self, f'_instruction_{instruction_name}')
        argument = int(argument)
        return instruction, argument

    def _instruction_nop(self, state: State, _: Any) -> None:
        state.cursor += 1

    def _instruction_acc(self, state: State, increment: int) -> None:
        state.accumulator += increment
        state.cursor += 1

    def _instruction_jmp(self, state: State, offset: int) -> None:
        state.cursor += offset

    def _run(self, patch_line: int = -1, patch_instruction: Callable = None) -> State:
        lines_done = set()
        state = State()
        while state.cursor != len(self._code):
            # Check for infinite loops
            if state.cursor in lines_done:
                state.success = False
                break
            lines_done.add(state.cursor)

            # Get the next instruction, patch it if necessary
            instruction, argument = self._code[state.cursor]
            if state.cursor == patch_line:
                instruction = patch_instruction

            # Let the instruction change the state
            instruction(state, argument)
        else:
            state.success = True
        return state


if __name__ == '__main__':
    import unittest
    from tests import TestBootloader  # pylint: disable=unused-import
    unittest.main(verbosity=3)
