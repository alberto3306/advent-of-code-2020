import unittest

from bootloader import Bootloader


class TestBootloader(unittest.TestCase):
    # pylint: disable=protected-access

    def test_run_with_fix(self):
        """
        When loading the code we try to fix it until it runs without infinite
        loops.
        """

        # Having code with an infinite loop
        lines = (
            'nop +0',
            'acc +1',
            'jmp +4',
            'acc +3',
            'jmp -3',
            'acc -99',
            'acc +1',
            'jmp -4',
            'acc +6',
        )

        # When the code is loaded and run
        loader = Bootloader(lines)
        result = loader.run_with_fix()

        # The result should be the accumulator value from the correct run
        self.assertEqual(result, 8)

    def test_run_fail(self):
        """
        Running code with an infinite loop returns false.
        """

        # Having code with an infinite loop
        lines = (
            'nop +0',
            'acc +1',
            'jmp +4',
            'acc +3',
            'jmp -3',
            'acc -99',
            'acc +1',
            'jmp -4',
            'acc +6',
        )

        # When running it without trying to fix it
        loader = Bootloader(lines)
        state = loader._run()

        # The state shouldn't be successful
        self.assertEqual(state.success, False)

        # And the accumulator should hold the correct value
        self.assertEqual(state.accumulator, 5)

    def test_run_success(self):
        """
        Running code without infinite loops returns true.
        """

        # Having code without infinite loops
        lines = (
            'nop +0',
            'acc +1',
            'jmp +4',
            'acc +3',
            'jmp -3',
            'acc -99',
            'acc +1',
            'nop -4',
            'acc +6',
        )

        # When running it without trying to fix it
        loader = Bootloader(lines)
        state = loader._run()

        # The state should be successful
        self.assertEqual(state.success, True)

        # And the accumulator should hold the correct value
        self.assertEqual(state.accumulator, 8)


if __name__ == '__main__':
    unittest.main(verbosity=3)
