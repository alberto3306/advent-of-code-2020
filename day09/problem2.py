#!/usr/bin/env python3

"""
--- Part Two ---

The final step in breaking the XMAS encryption relies on the invalid number you
just found: you must find a contiguous set of at least two numbers in your list
which sum to the invalid number from step 1.

Again consider the above example:

35 20 15 25 47 40 62 55 65 95 102 117 150 182 127 219 299 277 309 576

In this list, adding up all of the numbers from 15 through 40 produces the
invalid number from step 1, 127. (Of course, the contiguous set of numbers in
your actual list might be much longer.)

To find the encryption weakness, add together the smallest and largest number in
this contiguous range; in this example, these are 15 and 47, producing 62.

What is the encryption weakness in your XMAS-encrypted list of numbers?

"""

import os.path
import unittest
from typing import Iterable, Sequence


def solve() -> None:
    lines = load_input_file()
    numbers = tuple(map(int, lines))
    invalid = find_invalid_number(numbers)
    contiguous = find_contiguous_sum(numbers, invalid)
    result = min(contiguous) + max(contiguous)
    print(result)


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def find_invalid_number(numbers: Sequence[int], preamble_len: int = 25) -> int:
    for i in range(len(numbers)):
        preamble = numbers[i:i+preamble_len]
        number = numbers[i + preamble_len]
        if not is_number_valid(preamble, number):
            return number


def find_contiguous_sum(numbers: Iterable[int], total: int):
    for i in range(len(numbers)):
        for j in range(i, len(numbers)+1):
            chunk = numbers[i:j]
            chunk_sum = sum(chunk)
            if chunk_sum == total:
                return chunk
            if chunk_sum > total:
                break


def is_number_valid(preabmle: Sequence[int], number: int) -> bool:
    return any(
        a + b == number
        for a in preabmle
        for b in preabmle
        if a != b
    )


class Tests(unittest.TestCase):

    def test_find_invalid_number(self):
        preamble_len = 5
        numbers = (
            35, 20, 15, 25, 47,
            40, 62, 55, 65, 95,
            102, 117, 150, 182, 127,
            219, 299, 277, 309, 576,
        )
        result = find_invalid_number(numbers, preamble_len)
        self.assertEqual(result, 127)

    def test_is_number_valid(self):
        preamble1 = tuple(range(1, 26))
        preamble2 = tuple(range(1, 20)) + tuple(range(21, 26)) + (45,)
        cases = (
            (preamble1, 26, True),
            (preamble1, 49, True),
            (preamble1, 100, False),
            (preamble1, 50, False),
            (preamble2, 26, True),
            (preamble2, 65, False),
            (preamble2, 64, True),
            (preamble2, 66, True),
        )
        for preamble, number, expected in cases:
            result = is_number_valid(preamble, number)
            self.assertEqual(result, expected,
                             f'{preamble} with {number} should be {expected}')

    def test_find_contiguous_sum(self):
        numbers = (
            35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102,
            117, 150, 182, 127, 219, 299, 277, 309, 576,
        )
        total = 127
        result = find_contiguous_sum(numbers, total)
        expected = (15, 25, 47, 40)
        self.assertSequenceEqual(result, expected)


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    solve()
