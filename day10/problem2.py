#!/usr/bin/env python3

"""
--- Part Two ---

To completely determine whether you have enough adapters, you'll need to figure
out how many different ways they can be arranged. Every arrangement needs to
connect the charging outlet to your device. The previous rules about when
adapters can successfully connect still apply.

The first example above (the one that starts with 16, 10, 15) supports the
following arrangements:

    (0), 1, 4, 5, 6, 7, 10, 11, 12, 15, 16, 19, (22)
    (0), 1, 4, 5, 6, 7, 10, 12, 15, 16, 19, (22)
    (0), 1, 4, 5, 7, 10, 11, 12, 15, 16, 19, (22)
    (0), 1, 4, 5, 7, 10, 12, 15, 16, 19, (22)
    (0), 1, 4, 6, 7, 10, 11, 12, 15, 16, 19, (22)
    (0), 1, 4, 6, 7, 10, 12, 15, 16, 19, (22)
    (0), 1, 4, 7, 10, 11, 12, 15, 16, 19, (22)
    (0), 1, 4, 7, 10, 12, 15, 16, 19, (22)

(The charging outlet and your device's built-in adapter are shown in
parentheses.)

Given the adapters from the first example, the total number of
arrangements that connect the charging outlet to your device is 8.

The second example above (the one that starts with 28, 33, 18) has many
arrangements. Here are a few:

    (0), 1, 2, 3, 4, 7, 8, 9, 10, 11, 14, 17, 18, 19, 20, 23, 24, 25, 28, 31,
    32, 33, 34, 35, 38, 39, 42, 45, 46, 47, 48, 49, (52)

    (0), 1, 2, 3, 4, 7, 8, 9, 10, 11, 14, 17, 18, 19, 20, 23, 24, 25, 28, 31,
    32, 33, 34, 35, 38, 39, 42, 45, 46, 47, 49, (52)

    (0), 1, 2, 3, 4, 7, 8, 9, 10, 11, 14, 17, 18, 19, 20, 23, 24, 25, 28, 31,
    32, 33, 34, 35, 38, 39, 42, 45, 46, 48, 49, (52)

    (0), 1, 2, 3, 4, 7, 8, 9, 10, 11, 14, 17, 18, 19, 20, 23, 24, 25, 28, 31,
    32, 33, 34, 35, 38, 39, 42, 45, 46, 49, (52)

    (0), 1, 2, 3, 4, 7, 8, 9, 10, 11, 14, 17, 18, 19, 20, 23, 24, 25, 28, 31,
    32, 33, 34, 35, 38, 39, 42, 45, 47, 48, 49, (52)

    (0), 3, 4, 7, 10, 11, 14, 17, 20, 23, 25, 28, 31, 34, 35, 38, 39, 42, 45,
    46, 48, 49, (52)

    (0), 3, 4, 7, 10, 11, 14, 17, 20, 23, 25, 28, 31, 34, 35, 38, 39, 42, 45,
    46, 49, (52)

    (0), 3, 4, 7, 10, 11, 14, 17, 20, 23, 25, 28, 31, 34, 35, 38, 39, 42, 45,
    47, 48, 49, (52)

    (0), 3, 4, 7, 10, 11, 14, 17, 20, 23, 25, 28, 31, 34, 35, 38, 39, 42, 45,
    47, 49, (52)

    (0), 3, 4, 7, 10, 11, 14, 17, 20, 23, 25, 28, 31, 34, 35, 38, 39, 42, 45,
    48, 49, (52)

In total, this set of adapters can connect the charging outlet to your device in
19208 distinct arrangements.

You glance back down at your bag and try to remember why you brought so many
adapters; there must be more than a trillion valid ways to arrange them! Surely,
there must be an efficient way to count the arrangements.

What is the total number of distinct ways you can arrange the adapters to
connect the charging outlet to your device?

"""

import os.path
import unittest
from functools import lru_cache
from typing import Iterable, Tuple


def solve() -> None:
    lines = load_input_file()
    numbers = tuple(map(int, lines))
    print(count_all_combinations(numbers))


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def count_all_combinations(adapters: Iterable[int]) -> int:

    # Add the outer joltages
    adapters = tuple(adapters)
    adapters += (0, max(adapters) + 3)

    # Optimize by sorting first
    adapters = tuple(sorted(adapters))

    return count_branch_combinations(adapters)


@lru_cache(maxsize=None)
def count_branch_combinations(adapters: Tuple[int]) -> Iterable[Tuple[int]]:
    """
    Count recursively all the possible combinations for this branch.
    """

    if len(adapters) == 1:
        return 1

    result = 0
    current, *branch = adapters
    for pos, next_adapter in enumerate(branch, 1):
        if current < next_adapter <= current + 3:
            result += count_branch_combinations(adapters[pos:])
        else:
            break
    return result


class Tests(unittest.TestCase):

    maxDiff = None

    def test_count_all_combinations1(self):
        # Having an UNSORTED and NOT CLOSED sequence of adapters
        adapters = (
            16, 10, 15, 5, 1,
            11, 7, 19, 6, 12, 4,
        )

        # When counting the possible combinations
        result = count_all_combinations(adapters)

        # The result should be the expected
        self.assertEqual(result, 8)

    def test_count_all_combinations2(self):
        # Having an UNSORTED and NOT CLOSED sequence of adapters
        adapters = (
            28, 33, 18, 42, 31, 14, 46, 20, 48, 47,
            24, 23, 49, 45, 19, 38, 39, 11, 1, 32,
            25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3,
        )

        # When counting the possible combinations
        result = count_all_combinations(adapters)

        # The result should be the expected
        self.assertEqual(result, 19208)


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    solve()
