#!/usr/bin/env python3

"""
Your plane lands with plenty of time to spare. The final leg of your journey is
a ferry that goes directly to the tropical island where you can finally start
your vacation. As you reach the waiting area to board the ferry, you realize
you're so early, nobody else has even arrived yet!

By modeling the process people use to choose (or abandon) their seat in the
waiting area, you're pretty sure you can predict the best place to sit. You make
a quick map of the seat layout (your puzzle input).

The seat layout fits neatly on a grid. Each position is either floor (.), an
empty seat (L), or an occupied seat (#). For example, the initial seat layout
might look like this:

    L.LL.LL.LL
    LLLLLLL.LL
    L.L.L..L..
    LLLL.LL.LL
    L.LL.LL.LL
    L.LLLLL.LL
    ..L.L.....
    LLLLLLLLLL
    L.LLLLLL.L
    L.LLLLL.LL

Now, you just need to model the people who will be arriving shortly.
Fortunately, people are entirely predictable and always follow a simple set of
rules. All decisions are based on the number of occupied seats adjacent to a
given seat (one of the eight positions immediately up, down, left, right, or
diagonal from the seat). The following rules are applied to every seat
simultaneously:

    - If a seat is empty (L) and there are no occupied seats adjacent to it,
      the seat becomes occupied.
    - If a seat is occupied (#) and four or more seats adjacent to it are also
      occupied, the seat becomes empty.
    - Otherwise, the seat's state does not change.

Floor (.) never changes; seats don't move, and nobody sits on the floor.

After one round of these rules, every seat in the example layout becomes
occupied:

    #.##.##.##
    #######.##
    # .#.#..#..
    ####.##.##
    #.##.##.##
    #.#####.##
    ..#.#.....
    ##########
    #.######.#
    #.#####.##

After a second round, the seats with four or more occupied adjacent seats become
empty again:

    #.LL.L#.##
    #LLLLLL.L#
    L.L.L..L..
    #LLL.LL.L#
    # .LL.LL.LL
    #.LLLL#.##
    ..L.L.....
    #LLLLLLLL#
    # .LLLLLL.L
    #.#LLLL.##

This process continues for three more rounds:

    #.##.L#.##
    #L###LL.L#
    L.#.#..#..
    #L##.##.L#
    # .##.LL.LL
    #.###L#.##
    ..#.#.....
    #L######L#
    # .LL###L.L
    #.#L###.##

    #.#L.L#.##
    #LLL#LL.L#
    L.L.L..#..
    #LLL.##.L#
    # .LL.LL.LL
    #.LL#L#.##
    ..L.L.....
    #L#LLLL#L#
    # .LLLLLL.L
    #.#L#L#.##

    #.#L.L#.##
    #LLL#LL.L#
    L.#.L..#..
    #L##.##.L#
    # .#L.LL.LL
    #.#L#L#.##
    ..L.L.....
    #L#L##L#L#
    # .LLLLLL.L
    #.#L#L#.##

At this point, something interesting happens: the chaos stabilizes and further
applications of these rules cause no seats to change state! Once people stop
moving around, you count 37 occupied seats.

Simulate your seating area by applying the seating rules repeatedly until no
seats change state. How many seats end up occupied?
"""

import os.path
import unittest
from typing import Iterable, Sequence


EMPTY = 'L'
TAKEN = '#'
FLOOR = '.'
TAKEN_SEATS = 4


def solve() -> None:
    seats = tuple(load_input_file())
    taken_seats = run_simulation(seats)
    print(taken_seats)


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def run_simulation(seats: Sequence[str]) -> int:
    prev = ()
    while True:
        seats = next_step(seats)
        if prev == seats:
            break
        prev = seats
    return sum(row.count(TAKEN) for row in seats)


def next_step(seats: Sequence[str]) -> Sequence[str]:
    return tuple(
        ''.join(
            run_seat(seats, x, y)
            for y in range(len(seats[x]))
        )
        for x in range(len(seats))
    )


def run_seat(seats, x, y):

    seat = seats[x][y]
    if seat == FLOOR:
        return seat

    adjacent = get_adjacent(seats, x, y)

    if seat == EMPTY:
        if adjacent.count(TAKEN) == 0:
            return TAKEN

    if seat == TAKEN:
        if adjacent.count(TAKEN) >= 4:
            return EMPTY

    return seat


def get_adjacent(seats, x, y):
    deltas = (-1, 0, 1)
    return tuple(
        get_seat(seats, x+dx, y+dy)
        for dx in deltas
        for dy in deltas
        if dx or dy
    )


def get_seat(seats, x, y):
    if x < 0 or y < 0:
        return
    try:
        return seats[x][y]
    except IndexError:
        pass


class Tests(unittest.TestCase):

    start = (
        'L.LL.LL.LL',
        'LLLLLLL.LL',
        'L.L.L..L..',
        'LLLL.LL.LL',
        'L.LL.LL.LL',
        'L.LLLLL.LL',
        '..L.L.....',
        'LLLLLLLLLL',
        'L.LLLLLL.L',
        'L.LLLLL.LL',
    )

    def test_run_simulation(self):
        result = run_simulation(self.start)
        self.assertEqual(result, 37)

    def test_next_step(self):

        steps = (
            (
                '#.##.##.##',
                '#######.##',
                '#.#.#..#..',
                '####.##.##',
                '#.##.##.##',
                '#.#####.##',
                '..#.#.....',
                '##########',
                '#.######.#',
                '#.#####.##',
            ),
            (
                '#.LL.L#.##',
                '#LLLLLL.L#',
                'L.L.L..L..',
                '#LLL.LL.L#',
                '#.LL.LL.LL',
                '#.LLLL#.##',
                '..L.L.....',
                '#LLLLLLLL#',
                '#.LLLLLL.L',
                '#.#LLLL.##',
            ),
            (
                '#.##.L#.##',
                '#L###LL.L#',
                'L.#.#..#..',
                '#L##.##.L#',
                '#.##.LL.LL',
                '#.###L#.##',
                '..#.#.....',
                '#L######L#',
                '#.LL###L.L',
                '#.#L###.##',
            ),
            (
                '#.#L.L#.##',
                '#LLL#LL.L#',
                'L.L.L..#..',
                '#LLL.##.L#',
                '#.LL.LL.LL',
                '#.LL#L#.##',
                '..L.L.....',
                '#L#LLLL#L#',
                '#.LLLLLL.L',
                '#.#L#L#.##',
            ),
            (
                '#.#L.L#.##',
                '#LLL#LL.L#',
                'L.#.L..#..',
                '#L##.##.L#',
                '#.#L.LL.LL',
                '#.#L#L#.##',
                '..L.L.....',
                '#L#L##L#L#',
                '#.LLLLLL.L',
                '#.#L#L#.##',
            ),
            # The last step repeats indefinitely
            (
                '#.#L.L#.##',
                '#LLL#LL.L#',
                'L.#.L..#..',
                '#L##.##.L#',
                '#.#L.LL.LL',
                '#.#L#L#.##',
                '..L.L.....',
                '#L#L##L#L#',
                '#.LLLLLL.L',
                '#.#L#L#.##',
            ),
        )

        result = self.start
        for num, expected in enumerate(steps, 1):
            result = next_step(result)
            self.assertSequenceEqual(
                result[1:-1],
                expected[1:-1],
                f'Error in step {num}.'
            )

    def test_get_adjacent(self):

        seats = (
            'abc',
            'def',
            'ghi',
        )

        cases = (
            (0, 0, 'bed'),
            (0, 1, 'adefc'),
            (0, 2, 'bef'),
            (1, 0, 'abegh'),
            (1, 1, 'abcdfghi'),
            (1, 2, 'bcehi'),
            (2, 0, 'deh'),
            (2, 1, 'defgi'),
            (2, 2, 'efh'),
        )

        for x, y, expected in cases:
            result = get_adjacent(seats, x, y)
            self.assertSequenceEqual(
                sorted(filter(None, result)),
                sorted(expected),
            )


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    solve()
