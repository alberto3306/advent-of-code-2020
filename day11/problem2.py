#!/usr/bin/env python3

"""
--- Part Two ---

As soon as people start to arrive, you realize your mistake. People don't just
care about adjacent seats - they care about the first seat they can see in each
of those eight directions!

Now, instead of considering just the eight immediately adjacent seats, consider
the first seat in each of those eight directions. For example, the empty seat
below would see eight occupied seats:

.......#.
...#.....
.#.......
.........
..#L....#
....#....
.........
# ........
...#.....

The leftmost empty seat below would only see one empty seat, but cannot see any
of the occupied ones:

.............
.L.L.#.#.#.#.
.............

The empty seat below would see no occupied seats:

.##.##.
#.#.#.#
##...##
...L...
##...##
#.#.#.#
.##.##.

Also, people seem to be more tolerant than you expected: it now takes five or
more visible occupied seats for an occupied seat to become empty (rather than
four or more from the previous rules). The other rules still apply: empty seats
that see no occupied seats become occupied, seats matching no rule don't change,
and floor never changes.

Given the same starting layout as above, these new rules cause the seating area
to shift around as follows:

L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL

#.##.##.##
#######.##
# .#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##

#.LL.LL.L#
# LLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLL#
# .LLLLLL.L
#.LLLLL.L#

#.L#.##.L#
# L#####.LL
L.#.#..#..
##L#.##.##
#.##.#L.##
# .#####.#L
..#.#.....
LLL####LL#
# .L#####.L
#.L####.L#

#.L#.L#.L#
# LLLLLL.LL
L.L.L..#..
##LL.LL.L#
L.LL.LL.L#
# .LLLLL.LL
..L.L.....
LLLLLLLLL#
# .LLLLL#.L
#.L#LL#.L#

#.L#.L#.L#
# LLLLLL.LL
L.L.L..#..
##L#.#L.L#
L.L#.#L.L#
# .L####.LL
..#.#.....
LLL###LLL#
# .LLLLL#.L
#.L#LL#.L#

#.L#.L#.L#
# LLLLLL.LL
L.L.L..#..
##L#.#L.L#
L.L#.LL.L#
# .LLLL#.LL
..#.L.....
LLL###LLL#
# .LLLLL#.L
#.L#LL#.L#

Again, at this point, people stop shifting around and the seating area reaches
equilibrium. Once this occurs, you count 26 occupied seats.

Given the new visibility method and the rule change for occupied seats becoming
empty, once equilibrium is reached, how many seats end up occupied?

"""

import os.path
import unittest
from typing import Iterable, Sequence, Union

EMPTY = 'L'
TAKEN = '#'
FLOOR = '.'
TAKEN_SEATS = 5


def solve() -> None:
    seats = tuple(load_input_file())
    taken_seats = run_simulation(seats)
    print(taken_seats)


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def run_simulation(seats: Sequence[str]) -> int:
    prev = ()
    while True:
        seats = next_step(seats)
        if prev == seats:
            break
        prev = seats
    return sum(row.count(TAKEN) for row in seats)


def next_step(seats: Sequence[str]) -> Sequence[str]:
    return tuple(
        ''.join(
            run_seat(seats, x, y)
            for y in range(len(seats[x]))
        )
        for x in range(len(seats))
    )


def run_seat(seats, x, y):

    seat = seats[x][y]

    if seat == FLOOR:
        return seat

    adjacent = get_adjacent(seats, x, y)

    if seat == EMPTY:
        if adjacent.count(TAKEN) == 0:
            return TAKEN

    if seat == TAKEN:
        if adjacent.count(TAKEN) >= TAKEN_SEATS:
            return EMPTY

    return seat


def get_adjacent(seats, x, y):
    return tuple(
        get_seat(seats, x+dx, y+dy, dx, dy)
        for dx in (-1, 0, 1)
        for dy in (-1, 0, 1)
        if dx or dy
    )


def get_seat(seats, x, y, dx, dy) -> Union[str, None]:
    while True:
        if (x < 0) or (y < 0):
            return
        try:
            result = seats[x][y]
        except IndexError:
            return
        if result == FLOOR:
            x += dx
            y += dy
        else:
            return result


class Tests(unittest.TestCase):

    start = (
        'L.LL.LL.LL',
        'LLLLLLL.LL',
        'L.L.L..L..',
        'LLLL.LL.LL',
        'L.LL.LL.LL',
        'L.LLLLL.LL',
        '..L.L.....',
        'LLLLLLLLLL',
        'L.LLLLLL.L',
        'L.LLLLL.LL',
    )

    def test_run_simulation(self):
        result = run_simulation(self.start)
        self.assertEqual(result, 26)

    def test_next_step(self):

        steps = (
            (
                '#.##.##.##',
                '#######.##',
                '#.#.#..#..',
                '####.##.##',
                '#.##.##.##',
                '#.#####.##',
                '..#.#.....',
                '##########',
                '#.######.#',
                '#.#####.##',
            ),
            (
                '#.LL.LL.L#',
                '#LLLLLL.LL',
                'L.L.L..L..',
                'LLLL.LL.LL',
                'L.LL.LL.LL',
                'L.LLLLL.LL',
                '..L.L.....',
                'LLLLLLLLL#',
                '#.LLLLLL.L',
                '#.LLLLL.L#',
            ),
            (
                '#.L#.##.L#',
                '#L#####.LL',
                'L.#.#..#..',
                '##L#.##.##',
                '#.##.#L.##',
                '#.#####.#L',
                '..#.#.....',
                'LLL####LL#',
                '#.L#####.L',
                '#.L####.L#',
            ),
            (
                '#.L#.L#.L#',
                '#LLLLLL.LL',
                'L.L.L..#..',
                '##LL.LL.L#',
                'L.LL.LL.L#',
                '#.LLLLL.LL',
                '..L.L.....',
                'LLLLLLLLL#',
                '#.LLLLL#.L',
                '#.L#LL#.L#',
            ),
            (
                '#.L#.L#.L#',
                '#LLLLLL.LL',
                'L.L.L..#..',
                '##L#.#L.L#',
                'L.L#.#L.L#',
                '#.L####.LL',
                '..#.#.....',
                'LLL###LLL#',
                '#.LLLLL#.L',
                '#.L#LL#.L#',
            ),
            (
                '#.L#.L#.L#',
                '#LLLLLL.LL',
                'L.L.L..#..',
                '##L#.#L.L#',
                'L.L#.LL.L#',
                '#.LLLL#.LL',
                '..#.L.....',
                'LLL###LLL#',
                '#.LLLLL#.L',
                '#.L#LL#.L#',
            ),
            # The last step repeats indefinitely
            (
                '#.L#.L#.L#',
                '#LLLLLL.LL',
                'L.L.L..#..',
                '##L#.#L.L#',
                'L.L#.LL.L#',
                '#.LLLL#.LL',
                '..#.L.....',
                'LLL###LLL#',
                '#.LLLLL#.L',
                '#.L#LL#.L#',
            ),
        )

        result = self.start
        for num, expected in enumerate(steps, 1):
            result = next_step(result)
            self.assertSequenceEqual(
                result[1:-1],
                expected[1:-1],
                f'Error in step {num}.'
            )

    def test_get_adjacent(self):

        cases = (
            (
                (
                    '.......#.',
                    '...#.....',
                    '.#.......',
                    '.........',
                    '..#L....#',
                    '....#....',
                    '.........',
                    '#........',
                    '...#.....',
                ),
                4, 3,
                '########',
            ),
            (
                (
                    '.............',
                    '.L.L.#.#.#.#.',
                    '.............',
                ),
                1, 5,
                'L#',
            ),
            (
                (
                    '.##.##.',
                    '#.#.#.#',
                    '##...##',
                    '...L...',
                    '##...##',
                    '#.#.#.#',
                    '.##.##.',
                ),
                3, 3,
                '',
            )
        )

        for seats, x, y, expected in cases:
            result = get_adjacent(seats, x, y)
            self.assertSequenceEqual(
                sorted(filter(None, result)),
                sorted(expected),
            )


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    solve()
