#!/usr/bin/env python3

"""
--- Day 12: Rain Risk ---

Your ferry made decent progress toward the island, but the storm came in faster
than anyone expected. The ferry needs to take evasive actions!

Unfortunately, the ship's navigation computer seems to be malfunctioning; rather
than giving a route directly to safety, it produced extremely circuitous
instructions. When the captain uses the PA system to ask if anyone can help, you
quickly volunteer.

The navigation instructions (your puzzle input) consists of a sequence of
single-character actions paired with integer input values. After staring at them
for a few minutes, you work out what they probably mean:

 * Action N means to move north by the given value.
 * Action S means to move south by the given value.
 * Action E means to move east by the given value.
 * Action W means to move west by the given value.
 * Action L means to turn left the given number of degrees.
 * Action R means to turn right the given number of degrees.
 * Action F means to move forward by the given value in the direction the ship
   is currently facing.

The ship starts by facing east. Only the L and R actions change the direction
the ship is facing. (That is, if the ship is facing east and the next
instruction is N10, the ship would move north 10 units, but would still move
east if the following action were F.)

For example:

F10
N3
F7
R90
F11

These instructions would be handled as follows:

 * F10 would move the ship 10 units east (because the ship starts by facing
   east) to east 10, north 0.
 * N3 would move the ship 3 units north to east 10, north 3.
 * F7 would move the ship another 7 units east (because the ship is still facing
   east) to east 17, north 3.
 * R90 would cause the ship to turn right by 90 degrees and face south; it
   remains at east 17, north 3.
 * F11 would move the ship 11 units south to east 17, south 8.

At the end of these instructions, the ship's Manhattan distance (sum of the
absolute values of its east/west position and its north/south position) from its
starting position is 17 + 8 = 25.

Figure out where the navigation instructions lead. What is the Manhattan
distance between that location and the ship's starting position?

"""

import os.path
import unittest
from typing import Iterable, Sequence, Tuple

Heading = Tuple[int, int]
Position = Tuple[int, int]

NORTH: Heading = (0, 1)
SOUTH: Heading = (0, -1)
EAST: Heading = (1, 0)
WEST: Heading = (-1, 0)

LEFT = -1,
RIGHT = +1,

ANGLES = {
    0: NORTH,
    90: EAST,
    180: SOUTH,
    270: WEST,
}

REVERSE_ANGLES = {
    heading: angle
    for angle, heading in ANGLES.items()
}


def solve() -> None:
    steps = tuple(load_input_file())
    x, y = run_steps(steps)
    return abs(x) + abs(y)


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def run_steps(lines: Sequence[str]) -> Position:
    position = 0, 0
    heading = EAST
    steps = map(decode_step, lines)
    for step in steps:
        position, heading = next_step(position, heading, step)
    return position


def go_global(position_x: int, position_y: int,
              heading_x: int, heading_y: int,
              distance: int,
              direction_x: int, direction_y: int,
              ):
    return (
        position_x + (direction_x * distance),
        position_y + (direction_y * distance),
        heading_x,
        heading_y,
    )


def go_local(position_x, position_y,
             heading_x, heading_y,
             distance,
             *_
             ):
    return (
        position_x + (heading_x * distance),
        position_y + (heading_y * distance),
        heading_x,
        heading_y,
    )


def rotate(position_x, position_y,
           heading_x, heading_y,
           degrees, direction,
           ):

    current_angle = REVERSE_ANGLES[(heading_x, heading_y)]
    new_angle = current_angle + (degrees * direction)
    heading_x, heading_y = ANGLES[new_angle % 360]

    return (
        position_x,
        position_y,
        heading_x,
        heading_y,
    )


CODES = {
    'N': (go_global, NORTH),
    'S': (go_global, SOUTH),
    'E': (go_global, EAST),
    'W': (go_global, WEST),
    'L': (rotate, LEFT),
    'R': (rotate, RIGHT),
    'F': (go_local, NORTH),
}


def decode_step(line: str):
    code = line[0]
    value = int(line[1:])
    fn, mod = CODES[code]
    return fn, value, mod


def next_step(position: Position, heading: Heading, step):
    fn, value, mod = step
    x, y, hx, hy = fn(*position, *heading, value, *mod)
    return (x, y), (hx, hy)


class Tests(unittest.TestCase):

    def test_run_steps(self):
        steps = (
            'F10',
            'N3',
            'F7',
            'R90',
            'F11',
        )
        result = run_steps(steps)
        self.assertEqual(result, (17, -8))

    def test_next_step(self):

        steps = (
            ('F10', 10, 0, *EAST),
            ('N3', 10, 3, *EAST),
            ('F7', 17, 3, *EAST),
            ('R90', 17, 3, *SOUTH),
            ('F11', 17, -8, *SOUTH),
        )

        position = (0, 0)
        heading = EAST

        for line, *expected in steps:
            step = decode_step(line)
            prev_position = position
            prev_heading = heading
            position, heading = next_step(position, heading, step)
            self.assertSequenceEqual(
                (*position, *heading),
                expected,
                f'{prev_position},{prev_heading} + {line} = '
                f'{expected} != {position},{heading}')


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    print(solve())
