#!/usr/bin/env python3

"""
--- Part Two ---

Before you can give the destination to the captain, you realize that the actual
action meanings were printed on the back of the instructions the whole time.

Almost all of the actions indicate how to move a waypoint which is relative to
the ship's position:

 * Action N means to move the waypoint north by the given value.
 * Action S means to move the waypoint south by the given value.
 * Action E means to move the waypoint east by the given value.
 * Action W means to move the waypoint west by the given value.
 * Action L means to rotate the waypoint around the ship left
   (counter-clockwise) the given number of degrees.
 * Action R means to rotate the waypoint around the ship right (clockwise) the
   given number of degrees.
 * Action F means to move forward to the waypoint a number of times equal to the
   given value.

The waypoint starts 10 units east and 1 unit north relative to the ship. The
waypoint is relative to the ship; that is, if the ship moves, the waypoint moves
with it.

For example, using the same instructions as above:

 * F10 moves the ship to the waypoint 10 times (a total of 100 units east and 
   10 units north), leaving the ship at east 100, north 10. The waypoint stays 
   10 units east and 1 unit north of the ship.
 * N3 moves the waypoint 3 units north to 10 units east and 4 units north of 
   the ship. The ship remains at east 100, north 10.
 * F7 moves the ship to the waypoint 7 times (a total of 70 units east and 28 
   units north), leaving the ship at east 170, north 38. The waypoint stays 10 
   units east and 4 units north of the ship.
 * R90 rotates the waypoint around the ship clockwise 90 degrees, moving it to 
   4 units east and 10 units south of the ship. The ship remains at east 170, 
   north 38.
 * F11 moves the ship to the waypoint 11 times (a total of 44 units east and 
   110 units south), leaving the ship at east 214, south 72. The waypoint stays 
   4 units east and 10 units south of the ship.

After these operations, the ship's Manhattan distance from its starting position
is 214 + 72 = 286.

Figure out where the navigation instructions actually lead. What is the
Manhattan distance between that location and the ship's starting position?
"""

import os.path
import unittest
from typing import Iterable, Sequence, Tuple

Heading = Tuple[int, int]
Position = Tuple[int, int]

NORTH: Heading = (0, 1)
SOUTH: Heading = (0, -1)
EAST: Heading = (1, 0)
WEST: Heading = (-1, 0)

ANGLES = {
    90: lambda x, y: (y, -x),
    180: lambda x, y: (-x, -y),
    270: lambda x, y: (-y, x),
    -90: lambda x, y: (-y, x),
    -180: lambda x, y: (-x, -y),
    -270: lambda x, y: (y, -x),
}

LEFT = (-1,)
RIGHT = (+1,)


def solve() -> None:
    steps = tuple(load_input_file())
    x, y = run_steps(steps)
    return abs(x) + abs(y)


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def run_steps(lines: Sequence[str]) -> Position:
    position = 0, 0
    waypoint = 10, 1
    steps = map(decode_step, lines)
    for step in steps:
        position, waypoint = next_step(position, waypoint, step)
    return position


def move_waypoint(position_x: int,
                  position_y: int,
                  waypoint_x: int,
                  waypoint_y: int,
                  distance: int,
                  direction_x: int,
                  direction_y: int,
                  ):
    return (
        position_x,
        position_y,
        waypoint_x + (direction_x * distance),
        waypoint_y + (direction_y * distance),
    )


def move_ship(position_x: int,
              position_y: int,
              waypoint_x: int,
              waypoint_y: int,
              distance: int,
              *_):
    return (
        position_x + (waypoint_x * distance),
        position_y + (waypoint_y * distance),
        waypoint_x,
        waypoint_y,
    )


def rotate_waypoint(position_x: int,
                    position_y: int,
                    waypoint_x: int,
                    waypoint_y: int,
                    degrees: int,
                    direction: int,
                    ):

    waypoint_x, waypoint_y = ANGLES[degrees * direction](waypoint_x, waypoint_y)

    return (
        position_x,
        position_y,
        waypoint_x,
        waypoint_y,
    )


CODES = {
    'N': (move_waypoint, NORTH),
    'S': (move_waypoint, SOUTH),
    'E': (move_waypoint, EAST),
    'W': (move_waypoint, WEST),
    'L': (rotate_waypoint, LEFT),
    'R': (rotate_waypoint, RIGHT),
    'F': (move_ship, ()),
}


def decode_step(line: str):
    code = line[0]
    value = int(line[1:])
    fn, mod = CODES[code]
    return fn, value, mod


def next_step(position: Position, waypoint: Position, step):
    fn, value, mod = step
    x, y, hx, hy = fn(*position, *waypoint, value, *mod)
    return (x, y), (hx, hy)


class Tests(unittest.TestCase):

    def test_run_steps(self):
        steps = (
            'F10',
            'N3',
            'F7',
            'R90',
            'F11',
        )
        result = run_steps(steps)
        self.assertEqual(result, (214, -72))

    def test_next_step(self):

        steps = (
            ('F10', 100, 10, 10, 1),
            ('N3', 100, 10, 10, 4),
            ('F7', 170, 38, 10, 4),
            ('R90', 170, 38, 4, -10),
            ('F11', 214, -72, 4, -10),
        )

        position = (0, 0)
        waypoint = (10, 1)

        for line, *expected in steps:
            step = decode_step(line)
            prev_position = position
            prev_waypoint = waypoint
            position, waypoint = next_step(position, waypoint, step)
            self.assertSequenceEqual(
                (*position, *waypoint),
                expected,
                f'{prev_position},{prev_waypoint} + {line} = '
                f'{expected} != {position},{waypoint}'
            )


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    print(solve())
