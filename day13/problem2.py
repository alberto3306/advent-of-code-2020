#!/usr/bin/env python3

"""
--- Part Two ---

The shuttle company is running a contest: one gold coin for anyone that can find
the earliest timestamp such that the first bus ID departs at that time and each
subsequent listed bus ID departs at that subsequent minute. (The first line in
your input is no longer relevant.)

For example, suppose you have the same list of bus IDs as above:

7,13,x,x,59,x,31,19

An x in the schedule means there are no constraints on what bus IDs must depart
at that time.

This means you are looking for the earliest timestamp (called t) such that:

    Bus ID 7 departs at timestamp t.
    Bus ID 13 departs one minute after timestamp t.
    There are no requirements or restrictions on departures at two or three
    minutes after timestamp t.
    Bus ID 59 departs four minutes after timestamp t.
    There are no requirements or restrictions on departures at five minutes
    after timestamp t.
    Bus ID 31 departs six minutes after timestamp t.
    Bus ID 19 departs seven minutes after timestamp t.

The only bus departures that matter are the listed bus IDs at their specific
offsets from t. Those bus IDs can depart at other times, and other bus IDs can
depart at those times. For example, in the list above, because bus ID 19 must
depart seven minutes after the timestamp at which bus ID 7 departs, bus ID 7
will always also be departing with bus ID 19 at seven minutes after timestamp t.

In this example, the earliest timestamp at which this occurs is 1068781:

time     bus 7   bus 13  bus 59  bus 31  bus 19
1068773    .       .       .       .       .
1068774    D       .       .       .       .
1068775    .       .       .       .       .
1068776    .       .       .       .       .
1068777    .       .       .       .       .
1068778    .       .       .       .       .
1068779    .       .       .       .       .
1068780    .       .       .       .       .
1068781    D       .       .       .       .
1068782    .       D       .       .       .
1068783    .       .       .       .       .
1068784    .       .       .       .       .
1068785    .       .       D       .       .
1068786    .       .       .       .       .
1068787    .       .       .       D       .
1068788    D       .       .       .       D
1068789    .       .       .       .       .
1068790    .       .       .       .       .
1068791    .       .       .       .       .
1068792    .       .       .       .       .
1068793    .       .       .       .       .
1068794    .       .       .       .       .
1068795    D       D       .       .       .
1068796    .       .       .       .       .
1068797    .       .       .       .       .

In the above example, bus ID 7 departs at timestamp 1068788 (seven minutes after
t). This is fine; the only requirement on that minute is that bus ID 19 departs
then, and it does.

Here are some other examples:

    The earliest timestamp that matches the list 17,x,13,19 is 3417.
    67,7,59,61 first occurs at timestamp 754018.
    67,x,7,59,61 first occurs at timestamp 779210.
    67,7,x,59,61 first occurs at timestamp 1261476.
    1789,37,47,1889 first occurs at timestamp 1202161486.

However, with so many bus IDs in your list, surely the actual earliest timestamp
will be larger than 100000000000000!

What is the earliest timestamp such that all of the listed bus IDs depart at
offsets matching their positions in the list?
"""

import os.path
import unittest
from collections import Counter
from functools import lru_cache, reduce
from itertools import count
from operator import mul
from typing import Iterable, Sequence


def solve() -> None:
    _, busline = tuple(load_input_file())
    bus_ids = parse_busline(busline)
    return find_timestamp(bus_ids)


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def parse_busline(busline: str) -> Sequence[int]:
    return (
        int(x) if x != 'x' else 1
        for x in busline.split(',')
    )


def find_timestamp(bus_ids: Iterable[int]) -> int:
    buses = tuple(x or 1 for x in bus_ids)
    inc = buses[0]
    timestamp = buses[0]
    while True:
        for offset, bus in enumerate(buses):
            if (timestamp + offset) % bus:
                break
            inc = lcm(inc, bus)
        else:
            return timestamp
        timestamp += inc


@lru_cache(100)
def lcm(*nums: Iterable[int]) -> int:
    factors = {}
    for num in nums:
        for prime, exp in Counter(get_factors(num)).items():
            factors[prime] = max(factors.get(prime, 0), exp)
    return reduce(mul, (p**e for p, e in factors.items()))


def get_factors(num: int) -> Iterable[int]:
    for prime in primes():
        while (num // prime) * prime == num:
            yield(prime)
            num //= prime
        if num == 1:
            break


def primes() -> Iterable[int]:
    yield 2
    for num in count(3, 2):
        for div in range(3, num):
            if not num % div:
                break
        else:
            yield num


class Tests(unittest.TestCase):

    def test_parse_busline(self):
        line = '7,13,x,x,59,x,31,19'
        expected = (7, 13, 1, 1, 59, 1, 31, 19)
        result = parse_busline(line)
        self.assertSequenceEqual(tuple(result), expected)

    def test_find_timestamp(self):
        cases = (
            ((7, 13, 1, 1, 59, 1, 31, 19), 1068781),
            ((17, 1, 13, 19), 3417),
            ((67, 7, 59, 61), 754018),
            ((67, 1, 7, 59, 61), 779210),
            ((67, 7, 1, 59, 61), 1261476),
            ((1789, 37, 47, 1889), 1202161486),
        )
        for bus_ids, expected in cases:
            result = find_timestamp(bus_ids)
            self.assertEqual(result, expected, bus_ids)

    def test_lcm(self):
        result = lcm(72, 50)
        self.assertEqual(result, 1800)

    def test_get_factors(self):
        cases = (
            (72, 2, 2, 2, 3, 3),
            (50, 2, 5, 5),
            (864, 2, 2, 2, 2, 2, 3, 3, 3),
            (24, 2, 2, 2, 3),
            (90, 2, 3, 3, 5),
            (150, 2, 3, 5, 5),
        )
        for num, *expected in cases:
            result = get_factors(num)
            self.assertSequenceEqual(tuple(result), expected, num)

    def test_primes(self):
        prime = primes()
        self.assertEqual(next(prime), 2)
        self.assertEqual(next(prime), 3)
        self.assertEqual(next(prime), 5)
        self.assertEqual(next(prime), 7)
        self.assertEqual(next(prime), 11)
        self.assertEqual(next(prime), 13)
        self.assertEqual(next(prime), 17)


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    print(solve())
