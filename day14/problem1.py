#!/usr/bin/env python3

"""
--- Day 14: Docking Data ---

As your ferry approaches the sea port, the captain asks for your help again. The
computer system that runs this port isn't compatible with the docking program on
the ferry, so the docking parameters aren't being correctly initialized in the
docking program's memory.

After a brief inspection, you discover that the sea port's computer system uses
a strange bitmask system in its initialization program. Although you don't have
the correct decoder chip handy, you can emulate it in software!

The initialization program (your puzzle input) can either update the bitmask or
write a value to memory. Values and memory addresses are both 36-bit unsigned
integers. For example, ignoring bitmasks for a moment, a line like mem[8] = 11
would write the value 11 to memory address 8.

The bitmask is always given as a string of 36 bits, written with the most
significant bit (representing 2^35) on the left and the least significant bit
(2^0, that is, the 1s bit) on the right. The current bitmask is applied to
values immediately before they are written to memory: a 0 or 1 overwrites the
corresponding bit in the value, while an X leaves the bit in the value
unchanged.

For example, consider the following program:

mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0

This program starts by spechttps://meet.jit.si/InformalHomeworkResearchHystericallye value 73 is written to memory address 8 instead.
Then, the program tries to write 101 to address 7:

value:  000000000000000000000000000001100101  (decimal 101)
mask:   XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
result: 000000000000000000000000000001100101  (decimal 101)

This time, the mask has no effect, as the bits it overwrote were already the
values the mask tried to set. Finally, the program tries to write 0 to address
8:

value:  000000000000000000000000000000000000  (decimal 0)
mask:   XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
result: 000000000000000000000000000001000000  (decimal 64)

64 is written to address 8 instead, overwriting the value that was there
previously.

To initialize your ferry's docking program, you need the sum of all values left
in memory after the initialization program completes. (The entire 36-bit address
space begins initialized to the value 0 at every address.) In the above example,
only two values in memory are not zero - 101 (at address 7) and 64 (at address
8) - producing a sum of 165.

Execute the initialization program. What is the sum of all values left in memory
after it completes?

"""

import os.path
import re
import unittest
from typing import Iterable, Mapping, Tuple

Memory = Mapping[int, int]
Write = Tuple[int, int]
Mask = Tuple[int, int]


def solve() -> None:
    lines = load_input_file()
    memory = write_to_memory(lines)
    return sum(memory.values())


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def write_to_memory(lines: Iterable[str]) -> Memory:
    result = {}
    for address, value in decode_lines(lines):
        result[address] = value
    return result


MASK_EXPR = r'^mask = ([X10]+)$'
MEM_EXPR = r'mem\[(\d+)\] = (\d+)'


def decode_lines(lines: Iterable[str]) -> Write:
    m1 = 0
    m2 = 0
    for line in lines:
        if line.startswith('mask'):
            match = re.match(MASK_EXPR, line)
            mask, = match.groups()
            m1, m2 = decode_mask(mask)
        else:
            match = re.match(MEM_EXPR, line)
            address, value = match.groups()
            address = int(address)
            value = apply_mask(m1, m2, int(value))
            yield address, value


TRANS_M1 = str.maketrans('01X', '010')
TRANS_M2 = str.maketrans('01X', '100')


def decode_mask(mask: str) -> Mask:
    return (
        int(mask.translate(TRANS_M1), 2),
        int(mask.translate(TRANS_M2), 2),
    )


def apply_mask(m1, m2, value) -> int:
    return (value | m1) & ~m2


class Tests(unittest.TestCase):

    def test_write_to_memory(self):
        lines = (
            'mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X',
            'mem[8] = 11',
            'mem[7] = 101',
            'mem[8] = 0',
        )
        result = write_to_memory(lines)
        expected = (
            (7, 101),
            (8, 64),
        )
        self.assertSequenceEqual(sorted(result.items()), sorted(expected))
        self.assertEqual(sum(result.values()), 165)

    def test_apply_mask(self):

        mask = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X'

        cases = (
            (11, 73),
            (101, 101),
            (0, 64),
        )

        m1, m2 = decode_mask(mask)

        for value, expected in cases:
            result = apply_mask(m1, m2, value)
            self.assertEqual(result, expected,
                             f'\nValue:  {bin(value)}'
                             f'\nMask 1: {bin(m1)}'
                             f'\nMask 2: {bin(m2)}'
                             f'\nExpect: {bin(expected)}'
                             f'\nResult: {bin(result)}'
                             )

    def test_decode_mask(self):

        mask = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X'
        exp1 = 0b00000000000000000000000000001000000
        exp2 = 0b00000000000000000000000000000000010

        m1, m2 = decode_mask(mask)
        self.assertEqual(m1, exp1, f'\n{bin(exp1)}\n{bin(m1)}')
        self.assertEqual(m2, exp2, f'\n{bin(exp1)}\n{bin(m1)}')


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    print(solve())
