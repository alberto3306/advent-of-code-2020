#!/usr/bin/env python3

"""
--- Part Two ---

For some reason, the sea port's computer system still can't communicate with
your ferry's docking program. It must be using version 2 of the decoder chip!

A version 2 decoder chip doesn't modify the values being written at all.
Instead, it acts as a memory address decoder. Immediately before a value is
written to memory, each bit in the bitmask modifies the corresponding bit of the
destination memory address in the following way:

    If the bitmask bit is 0, the corresponding memory address bit is unchanged.
    If the bitmask bit is 1, the corresponding memory address bit is overwritten with 1.
    If the bitmask bit is X, the corresponding memory address bit is floating.

A floating bit is not connected to anything and instead fluctuates
unpredictably. In practice, this means the floating bits will take on all
possible values, potentially causing many memory addresses to be written all at
once!

For example, consider the following program:

mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1

When this program goes to write to memory address 42, it first applies the
bitmask:

address: 000000000000000000000000000000101010  (decimal 42)
mask:    000000000000000000000000000000X1001X
result:  000000000000000000000000000000X1101X

After applying the mask, four bits are overwritten, three of which are
different, and two of which are floating. Floating bits take on every possible
combination of values; with two floating bits, four actual memory addresses are
written:

000000000000000000000000000000011010  (decimal 26)
000000000000000000000000000000011011  (decimal 27)
000000000000000000000000000000111010  (decimal 58)
000000000000000000000000000000111011  (decimal 59)

Next, the program is about to write to memory address 26 with a different
bitmask:

address: 000000000000000000000000000000011010  (decimal 26)
mask:    00000000000000000000000000000000X0XX
result:  00000000000000000000000000000001X0XX

This results in an address with three floating bits, causing writes to eight
memory addresses:

000000000000000000000000000000010000  (decimal 16)
000000000000000000000000000000010001  (decimal 17)
000000000000000000000000000000010010  (decimal 18)
000000000000000000000000000000010011  (decimal 19)
000000000000000000000000000000011000  (decimal 24)
000000000000000000000000000000011001  (decimal 25)
000000000000000000000000000000011010  (decimal 26)
000000000000000000000000000000011011  (decimal 27)

The entire 36-bit address space still begins initialized to the value 0 at every
address, and you still need the sum of all values left in memory at the end of
the program. In this example, the sum is 208.

Execute the initialization program using an emulator for a version 2 decoder
chip. What is the sum of all values left in memory after it completes?

"""

import os.path
import re
import unittest
from functools import reduce
from itertools import combinations
from operator import or_
from typing import Iterable, Mapping, Tuple

Memory = Mapping[int, int]
Write = Tuple[int, int]
Mask = Tuple[int, int]


def solve() -> None:
    lines = load_input_file()
    memory = write_to_memory(lines)
    return sum(memory.values())


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def write_to_memory(lines: Iterable[str]) -> Memory:
    result = {}
    for address, value in decode_lines(lines):
        result[address] = value
    return result


MASK_EXPR = r'^mask = ([X10]+)$'
MEM_EXPR = r'mem\[(\d+)\] = (\d+)'


def decode_lines(lines: Iterable[str]) -> Write:
    for line in lines:
        if line.startswith('mask'):
            match = re.match(MASK_EXPR, line)
            mask, = match.groups()
            mask = decode_mask(mask)
        else:
            match = re.match(MEM_EXPR, line)
            address, value = match.groups()
            value = int(value)
            for address in apply_mask(*mask,  int(address)):
                yield address, value


TRANS_M1 = str.maketrans('01X', '010')
TRANS_M2 = str.maketrans('01X', '001')


def decode_mask(mask: str) -> Mask:
    return (
        int(mask.translate(TRANS_M1), 2),
        int(mask.translate(TRANS_M2), 2),
    )


def apply_mask(ones, exes, value) -> Iterable[int]:
    return (
        (value | ones) ^ mask
        for mask in generate_variations(exes)
    )


def generate_variations(exes):
    bits = (
        2**exp
        for exp in range(36)
    )
    floating_bits = tuple(
        bit for bit in bits
        if bit | exes == exes
    )
    yield 0
    for i in range(len(floating_bits)):
        for comb in combinations(floating_bits, i+1):
            yield reduce(or_, comb)


class Tests(unittest.TestCase):

    def test_write_to_memory(self):

        lines = (
            'mask = 000000000000000000000000000000X1001X',
            'mem[42] = 100',
            'mask = 00000000000000000000000000000000X0XX',
            'mem[26] = 1',
        )

        result = write_to_memory(lines)
        self.assertEqual(sum(result.values()), 208)

    def test_apply_mask(self):

        cases = (
            (42, '000000000000000000000000000000X1001X', (26, 27, 58, 59)),
            (26, '00000000000000000000000000000000X0XX', (16, 17, 18, 19, 24, 25, 26, 27)),
        )

        for value, mask, expected in cases:
            decoded_mask = decode_mask(mask)
            result = apply_mask(*decoded_mask, value)
            self.assertEqual(sorted(result), sorted(expected))

    def test_decode_mask(self):

        mask = '000000000000000000000000000000X1001X'
        ones = 0b00000000000000000000000000000010010
        exes = 0b00000000000000000000000000000100001

        m1, m2 = decode_mask(mask)
        self.assertEqual(m1, ones, f'\n{bin(ones)}\n{bin(m1)}')
        self.assertEqual(m2, exes, f'\n{bin(exes)}\n{bin(m2)}')


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    print(solve())
