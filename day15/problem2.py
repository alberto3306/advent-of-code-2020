#!/usr/bin/env python3

"""
--- Part Two ---

Impressed, the Elves issue you a challenge: determine the 30000000th number
spoken. For example, given the same starting numbers as above:

    Given 0,3,6, the 30000000th number spoken is 175594.
    Given 1,3,2, the 30000000th number spoken is 2578.
    Given 2,1,3, the 30000000th number spoken is 3544142.
    Given 1,2,3, the 30000000th number spoken is 261214.
    Given 2,3,1, the 30000000th number spoken is 6895259.
    Given 3,2,1, the 30000000th number spoken is 18.
    Given 3,1,2, the 30000000th number spoken is 362.

Given your starting numbers, what will be the 30000000th number spoken?

"""

import os.path
import unittest
from typing import Iterable


def solve() -> None:
    numbers = load_input_file()
    return get_turn(numbers, 30000000)


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(int, fd.read().strip().split(','))


def get_turn(numbers: Iterable[int], turn: int) -> int:
    last_seen = [0] * turn
    i = 0
    number = None
    for i, number in enumerate(numbers, 1):
        last_seen[number] = i
    for i in range(i, turn):
        last = last_seen[number]
        last_seen[number] = i
        number = last and i - last
    return number


class Tests(unittest.TestCase):

    def test_get_turn(self):
        cases = (
            (0, 3, 6, 2020, 436),
            (1, 3, 2, 2020, 1),
            (2, 1, 3, 2020, 10),
            (1, 2, 3, 2020, 27),
            (2, 3, 1, 2020, 78),
            (3, 2, 1, 2020, 438),
            (3, 1, 2, 2020, 1836),
            (0, 3, 6, 30000000, 175594),
            (1, 3, 2, 30000000, 2578),
            (2, 1, 3, 30000000, 3544142),
            (1, 2, 3, 30000000, 261214),
            (2, 3, 1, 30000000, 6895259),
            (3, 2, 1, 30000000, 18),
            (3, 1, 2, 30000000, 362),
        )
        for *numbers, turn, expected in cases:
            result = get_turn(numbers, turn)
            self.assertEqual(result, expected)


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    print(solve())
