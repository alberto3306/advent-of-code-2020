#!/usr/bin/env python3

"""
--- Day 16: Ticket Translation ---

As you're walking to yet another connecting flight, you realize that one of the
legs of your re-routed trip coming up is on a high-speed train. However, the
train ticket you were given is in a language you don't understand. You should
probably figure out what it says before you get to the train station after the
next flight.

Unfortunately, you can't actually read the words on the ticket. You can,
however, read the numbers, and so you figure out the fields these tickets must
have and the valid ranges for values in those fields.

You collect the rules for ticket fields, the numbers on your ticket, and the
numbers on other nearby tickets for the same train service (via the airport
security cameras) together into a single document you can reference (your puzzle
input).

The rules for ticket fields specify a list of fields that exist somewhere on the
ticket and the valid ranges of values for each field. For example, a rule like
class: 1-3 or 5-7 means that one of the fields in every ticket is named class
and can be any value in the ranges 1-3 or 5-7 (inclusive, such that 3 and 5 are
both valid in this field, but 4 is not).

Each ticket is represented by a single line of comma-separated values. The
values are the numbers on the ticket in the order they appear; every ticket has
the same format. For example, consider this ticket:

.--------------------------------------------------------.
| ????: 101    ?????: 102   ??????????: 103     ???: 104 |
|                                                        |
| ??: 301  ??: 302             ???????: 303      ??????? |
| ??: 401  ??: 402           ???? ????: 403    ????????? |
'--------------------------------------------------------'

Here, ? represents text in a language you don't understand. This ticket might be
represented as 101,102,103,104,301,302,303,401,402,403; of course, the actual
train tickets you're looking at are much more complicated. In any case, you've
extracted just the numbers in such a way that the first number is always the
same specific field, the second number is always a different specific field, and
so on - you just don't know what each position actually means!

Start by determining which tickets are completely invalid; these are tickets
that contain values which aren't valid for any field. Ignore your ticket for
now.

For example, suppose you have the following notes:

class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12

It doesn't matter which position corresponds to which field; you can identify
invalid nearby tickets by considering only whether tickets contain values that
are not valid for any field. In this example, the values on the first nearby
ticket are all valid for at least one field. This is not true of the other three
nearby tickets: the values 4, 55, and 12 are are not valid for any field. Adding
together all of the invalid values produces your ticket scanning error rate: 4 +
55 + 12 = 71.

Consider the validity of the nearby tickets you scanned. What is your ticket
scanning error rate?

"""

import re
import os.path
import unittest
from typing import Iterable, Mapping, Sequence, Tuple

Rules = Mapping[str, Sequence[Tuple[int, int]]]
Ticket = Sequence[int]


def solve() -> None:
    lines = load_input_file()
    rules, _, nearby = parse_lines(lines)
    invalid = find_nearby_invalid_fields(rules, nearby)
    return sum(invalid)


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


RULES_EXPR = r'(.+)\: (\d+)\-(\d+) or (\d+)\-(\d+)'


def parse_lines(lines: Iterable[str]) -> Tuple[Rules, Ticket, Iterable[Ticket]]:

    lines = iter(lines)

    rules = {}
    for line in lines:
        if not line:
            break
        match = re.match(RULES_EXPR, line)
        field, a, b, c, d = match.groups()
        a = int(a)
        b = int(b)
        c = int(c)
        d = int(d)
        rules[field] = ((a, b), (c, d))

    assert next(lines) == 'your ticket:'
    ticket = tuple(map(int, next(lines).split(',')))

    assert next(lines) == ''
    assert next(lines) == 'nearby tickets:'
    nearby = (
        tuple(map(int, line.split(',')))
        for line in lines
    )

    return rules, ticket, nearby


def find_nearby_invalid_fields(rules: Rules, tickets: Iterable[Ticket]) -> Iterable[int]:
    for ticket in tickets:
        yield from find_ticket_invalid_fields(rules, ticket)


def find_ticket_invalid_fields(rules: Rules, ticket: Ticket) -> Iterable[int]:
    for value in ticket:
        for ranges in rules.values():
            for a, b in ranges:
                if a <= value <= b:
                    break
            else:
                continue
            break
        else:
            yield value


class Tests(unittest.TestCase):

    lines = (
        'class: 1-3 or 5-7',
        'row: 6-11 or 33-44',
        'seat: 13-40 or 45-50',
        '',
        'your ticket:',
        '7,1,14',
        '',
        'nearby tickets:',
        '7,3,47',
        '40,4,50',
        '55,2,20',
        '38,6,12',
    )

    rules = {
        'class': ((1, 3), (5, 7)),
        'row': ((6, 11), (33, 44)),
        'seat': ((13, 40), (45, 50)),
    }

    ticket = (7, 1, 14)

    nearby = (
        (7, 3, 47),
        (40, 4, 50),
        (55, 2, 20),
        (38, 6, 12),
    )

    def test_parse_lines(self):
        rules, ticket, nearby = parse_lines(self.lines)
        self.assertDictEqual(rules, self.rules)
        self.assertSequenceEqual(ticket, self.ticket)
        self.assertSequenceEqual(tuple(nearby), self.nearby)

    def test_find_nearby_invalid_fields(self):
        result = find_nearby_invalid_fields(self.rules, self.nearby)
        expected = (4, 55, 12)
        self.assertSequenceEqual(tuple(result), expected)


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    print(solve())
