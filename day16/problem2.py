#!/usr/bin/env python3

"""
--- Part Two ---

Now that you've identified which tickets contain invalid values, discard those
tickets entirely. Use the remaining valid tickets to determine which field is
which.

Using the valid ranges for each field, determine what order the fields appear on
the tickets. The order is consistent between all tickets: if seat is the third
field, it is the third field on every ticket, including your ticket.

For example, suppose you have the following notes:

class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9

Based on the nearby tickets in the above example, the first position must be
row, the second position must be class, and the third position must be seat; you
can conclude that in your ticket, class is 12, row is 11, and seat is 13.

Once you work out which field is which, look for the six fields on your ticket
that start with the word departure. What do you get if you multiply those six
values together?
"""

import os.path
import re
import unittest
from functools import reduce
from operator import mul
from typing import Iterable, Mapping, Sequence, Tuple

Rules = Mapping[str, Sequence[Tuple[int, int]]]
Ticket = Sequence[int]


def solve() -> None:
    lines = load_input_file()
    rules, ticket, nearby = parse_lines(lines)
    nearby = filter_invalid_tickets(rules, nearby)
    field_order = find_field_order(rules, ticket, nearby)
    ticket_fields = zip(field_order, ticket)
    departures = tuple(
        value
        for field, value in ticket_fields
        if field.startswith('departure')
    )
    return reduce(mul, departures)


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


RULES_EXPR = r'(.+)\: (\d+)\-(\d+) or (\d+)\-(\d+)'


def parse_lines(lines: Iterable[str]) -> Tuple[Rules, Ticket, Iterable[Ticket]]:

    lines = iter(lines)

    rules = {}
    for line in lines:
        if not line:
            break
        match = re.match(RULES_EXPR, line)
        field, a, b, c, d = match.groups()
        a = int(a)
        b = int(b)
        c = int(c)
        d = int(d)
        rules[field] = ((a, b), (c, d))

    assert next(lines) == 'your ticket:'
    ticket = tuple(map(int, next(lines).split(',')))

    assert next(lines) == ''
    assert next(lines) == 'nearby tickets:'
    nearby = (
        tuple(map(int, line.split(',')))
        for line in lines
    )

    return rules, ticket, nearby


def filter_invalid_tickets(rules: Rules, tickets: Iterable[Ticket]) -> Iterable[Ticket]:
    for ticket in tickets:
        if is_valid_ticket(rules, ticket):
            yield ticket


def find_field_order(rules: Rules, my_ticket: Ticket, nearby: Iterable[Ticket]):

    valid_positions = {
        field: set(range(len(my_ticket)))
        for field in rules
    }

    # Discard positions from fields where rules dont match
    for nearby_ticket in nearby:
        for pos, value in enumerate(nearby_ticket):
            for field in find_invalid_fields(rules, value):
                valid_positions[field].remove(pos)

    # Discard positions that are the only choice for other field
    done = set()
    while True:
        # Get all the positions that are in their definitive field
        to_remove = set()
        for valid_position in valid_positions.values():
            if len(valid_position) == 1:
                to_remove |= valid_position

        # Remove them from other fields if not done before
        to_remove -= done
        if to_remove:
            for valid_position in valid_positions.values():
                if len(valid_position) != 1:
                    valid_position -= to_remove
        else:
            # We're done
            break

        # Repeat again until no more changes are necessary
        done |= to_remove

    # Sort the fields according to its position
    positions = sorted(
        (pos, field)
        for field, (pos,) in valid_positions.items()
    )
    return tuple(field for _, field in positions)


def is_valid_ticket(rules: Rules, ticket: Ticket):
    for value in ticket:
        invalid_fields = find_invalid_fields(rules, value)
        if len(tuple(invalid_fields)) == len(rules):
            return False
    return True


def find_invalid_fields(rules: Rules, value: int) -> Iterable[str]:
    for field, ranges in rules.items():
        for a, b in ranges:
            if a <= value <= b:
                break
        else:
            yield field


class Tests(unittest.TestCase):

    def test_parse_lines(self):

        lines = (
            'class: 1-3 or 5-7',
            'row: 6-11 or 33-44',
            'seat: 13-40 or 45-50',
            '',
            'your ticket:',
            '7,1,14',
            '',
            'nearby tickets:',
            '7,3,47',
            '40,4,50',
            '55,2,20',
            '38,6,12',
        )

        rules, ticket, nearby = parse_lines(lines)

        expected_rules = {
            'class': ((1, 3), (5, 7)),
            'row': ((6, 11), (33, 44)),
            'seat': ((13, 40), (45, 50)),
        }
        expected_ticket = (7, 1, 14)
        expected_nearby = (
            (7, 3, 47),
            (40, 4, 50),
            (55, 2, 20),
            (38, 6, 12),
        )

        self.assertDictEqual(rules, expected_rules)
        self.assertSequenceEqual(ticket, expected_ticket)
        self.assertSequenceEqual(tuple(nearby), expected_nearby)

    def test_filter_invalid_tickets(self):

        rules = {
            'class': ((1, 3), (5, 7)),
            'row': ((6, 11), (33, 44)),
            'seat': ((13, 40), (45, 50)),
        }
        nearby = (
            (7, 3, 47),
            (40, 4, 50),
            (55, 2, 20),
            (38, 6, 12),
        )

        result = filter_invalid_tickets(rules, nearby)

        expected = (
            (7, 3, 47),
        )
        self.assertEqual(tuple(result), expected)

    def test_find_field_order(self):

        rules = {
            'class': ((0, 1), (4, 19)),
            'row': ((0, 5), (8, 19)),
            'seat': ((0, 13), (16, 19)),
        }
        ticket = (1, 2, 3)
        nearby = (
            (3, 9, 18),
            (15, 1, 5),
            (5, 14, 9),
        )

        result = find_field_order(rules, ticket, nearby)

        expected = (
            'row',
            'class',
            'seat',
        )
        self.assertSequenceEqual(result, expected)


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    print(solve())
