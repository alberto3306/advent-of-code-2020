#!/usr/bin/env python3

"""
--- Day 17: Conway Cubes ---

As your flight slowly drifts through the sky, the Elves at the Mythical
Information Bureau at the North Pole contact you. They'd like some help
debugging a malfunctioning experimental energy source aboard one of their
super-secret imaging satellites.

The experimental energy source is based on cutting-edge technology: a set of
Conway Cubes contained in a pocket dimension! When you hear it's having
problems, you can't help but agree to take a look.

The pocket dimension contains an infinite 3-dimensional grid. At every integer
3-dimensional coordinate (x,y,z), there exists a single cube which is either
active or inactive.

In the initial state of the pocket dimension, almost all cubes start inactive.
The only exception to this is a small flat region of cubes (your puzzle input);
the cubes in this region start in the specified active (#) or inactive (.)
state.

The energy source then proceeds to boot up by executing six cycles.

Each cube only ever considers its neighbors: any of the 26 other cubes where any
of their coordinates differ by at most 1. For example, given the cube at
x=1,y=2,z=3, its neighbors include the cube at x=2,y=2,z=2, the cube at
x=0,y=2,z=3, and so on.

During a cycle, all cubes simultaneously change their state according to the
following rules:

 * If a cube is active and exactly 2 or 3 of its neighbors are also active, the
   cube remains active. Otherwise, the cube becomes inactive.
 * If a cube is inactive but exactly 3 of its neighbors are active, the cube
   becomes active. Otherwise, the cube remains inactive.

The engineers responsible for this experimental energy source would like you to
simulate the pocket dimension and determine what the configuration of cubes
should be at the end of the six-cycle boot process.

For example, consider the following initial state:

.#.
..#
###

Even though the pocket dimension is 3-dimensional, this initial state represents
a small 2-dimensional slice of it. (In particular, this initial state defines a
3x3x1 region of the 3-dimensional space.)

Simulating a few cycles from this initial state produces the following
configurations, where the result of each cycle is shown layer-by-layer at each
given z coordinate (and the frame of view follows the active cells in each
cycle):

Before any cycles:

z=0
.#.
..#
###


After 1 cycle:

z=-1
# ..
..#
.#.

z=0
#.#
.##
.#.

z=1
# ..
..#
.#.


After 2 cycles:

z=-2
.....
.....
..#..
.....
.....

z=-1
..#..
.#..#
....#
.#...
.....

z=0
# ...
# ...
# ....
....#
.###.

z=1
..#..
.#..#
....#
.#...
.....

z=2
.....
.....
..#..
.....
.....


After 3 cycles:

z=-2
.......
.......
..##...
..###..
.......
.......
.......

z=-1
..#....
...#...
# ......
.....##
.#...#.
..#.#..
...#...

z=0
...#...
.......
# ......
.......
.....##
.##.#..
...#...

z=1
..#....
...#...
# ......
.....##
.#...#.
..#.#..
...#...

z=2
.......
.......
..##...
..###..
.......
.......
.......

After the full six-cycle boot process completes, 112 cubes are left in the
active state.

Starting with your given initial configuration, simulate six cycles. How many
cubes are left in the active state after the sixth cycle?

"""

import os.path
import unittest
from typing import Iterable, Set, Tuple

Cube = Tuple[int, int, int]
Universe = Set[Cube]

NEIGHBOR_DISTANCE = 1


def solve() -> None:
    lines = load_input_file()
    universe = parse_lines(lines)
    for _ in range(6):
        universe = iterate(universe)
    return len(universe)


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def parse_lines(lines: Iterable[str], x=0, y=0, z=0) -> Universe:
    return set(
        (xx, yy, z)
        for xx, row in enumerate(lines, x)
        for yy, col in enumerate(row, y)
        if col == '#'
    )


def build_lines(universe: Universe) -> Iterable[str]:

    (min_x, min_y, min_z), (max_x, max_y, max_z) = find_limits(universe)

    result = []
    for z in range(min_z, max_z + 1):
        level = []
        for x in range(min_x, max_x + 1):
            level.append(''.join((
                '#' if (x, y, z) in universe else '.'
                for y in range(min_y, max_y + 1)
            )))
        result.append(tuple(level))
    return result


def iterate(universe: Universe) -> Universe:
    result = set()
    for cube in get_all_cubes(universe):
        active = cube in universe
        num_active_neighbors = len(get_neighbors(*cube) & universe)
        if active:
            if 2 <= num_active_neighbors <= 3:
                result.add(cube)
        else:
            if num_active_neighbors == 3:
                result.add(cube)
    return result


def get_all_cubes(universe: Universe) -> Universe:

    (min_x, min_y, min_z), (max_x, max_y, max_z) = find_limits(universe)

    return (
        (x, y, z)
        for x in range(min_x - NEIGHBOR_DISTANCE, max_x + NEIGHBOR_DISTANCE + 1)
        for y in range(min_y - NEIGHBOR_DISTANCE, max_y + NEIGHBOR_DISTANCE + 1)
        for z in range(min_z - NEIGHBOR_DISTANCE, max_z + NEIGHBOR_DISTANCE + 1)
    )


def get_neighbors(x: int, y: int, z: int) -> Universe:
    return set(
        (xx, yy, zz)
        for xx in (x - NEIGHBOR_DISTANCE, x, x + NEIGHBOR_DISTANCE)
        for yy in (y - NEIGHBOR_DISTANCE, y, y + NEIGHBOR_DISTANCE)
        for zz in (z - NEIGHBOR_DISTANCE, z, z + NEIGHBOR_DISTANCE)
        if not (xx == x and yy == y and zz == z)
    )


def find_limits(universe: Universe) -> Tuple[Cube, Cube]:
    return (
        (
            min(x for x, _, _ in universe),
            min(y for _, y, _ in universe),
            min(z for _, _, z in universe),
        ),
        (
            max(x for x, _, _ in universe),
            max(y for _, y, _ in universe),
            max(z for _, _, z in universe),
        ),
    )


class Tests(unittest.TestCase):

    lines = (
        '.#.',
        '..#',
        '###',
    )

    universe = (
        (0, 1, 0),
        (1, 2, 0),
        (2, 0, 0),
        (2, 1, 0),
        (2, 2, 0),
    )

    iterations = (
        (
            set()
            | parse_lines(('#..', '..#', '.#.'), z=-1)
            | parse_lines(('#.#', '.##', '.#.'), z=0)
            | parse_lines(('#..', '..#', '.#.'), z=1)
        ),
        (
            set()
            | parse_lines(('.....', '.....', '..#..', '.....', '.....'), x=-1, y=-1, z=-2)
            | parse_lines(('..#..', '.#..#', '....#', '.#...', '.....'), x=-1, y=-1, z=-1)
            | parse_lines(('##...', '##...', '#....', '....#', '.###.'), x=-1, y=-1, z=0)
            | parse_lines(('..#..', '.#..#', '....#', '.#...', '.....'), x=-1, y=-1, z=1)
            | parse_lines(('.....', '.....', '..#..', '.....', '.....'), x=-1, y=-1, z=2)

        ),
        (
            set()
            | parse_lines(('.......', '.......', '..##...', '..###..', '.......', '.......', '.......'), z=-2)
            | parse_lines(('..#....', '...#...', '#......', '.....##', '.#...#.', '..#.#..', '...#...'), z=-1)
            | parse_lines(('...#...', '.......', '#......', '.......', '.....##', '.##.#..', '...#...'), z=0)
            | parse_lines(('..#....', '...#...', '#......', '.....##', '.#...#.', '..#.#..', '...#...'), z=1)
            | parse_lines(('.......', '.......', '..##...', '..###..', '.......', '.......', '.......'), z=2)
        ),
    )

    def test_parse_lines(self):
        result = parse_lines(self.lines)
        self.assertSequenceEqual(sorted(result), sorted(self.universe))

    def test_build_lines(self):
        universe = parse_lines(self.lines)
        result = build_lines(universe)
        self.assertSequenceEqual(result, [self.lines])

    def test_iterate_steps(self):
        result = set(self.universe)
        for num, expected in enumerate(self.iterations, 1):
            result = iterate(result)
            self.assertEqual(len(result), len(expected))
            self.assertSequenceEqual(
                build_lines(result),
                build_lines(expected),
                f'Error in iteration {num}.'
            )

    def test_iterate_result(self):
        universe = set(self.universe)
        for _ in range(6):
            universe = iterate(universe)
        self.assertEqual(len(universe), 112)

    def test_get_neighbors(self):
        cube = (0, 0, 0)
        result = tuple(get_neighbors(*cube))
        self.assertEqual(len(result), 26)
        expected = tuple(
            (x, y, z)
            for x in (-1, 0, 1)
            for y in (-1, 0, 1)
            for z in (-1, 0, 1)
            if (x, y, z) != (0, 0, 0)
        )
        self.assertSequenceEqual(sorted(result), sorted(expected))


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    print(solve())
