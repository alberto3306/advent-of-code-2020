#!/usr/bin/env python3

"""
--- Part Two ---

For some reason, your simulated results don't match what the experimental energy
source engineers expected. Apparently, the pocket dimension actually has four
spatial dimensions, not three.

The pocket dimension contains an infinite 4-dimensional grid. At every integer
4-dimensional coordinate (x,y,z,w), there exists a single cube (really, a
hypercube) which is still either active or inactive.

Each cube only ever considers its neighbors: any of the 80 other cubes where any
of their coordinates differ by at most 1. For example, given the cube at
x=1,y=2,z=3,w=4, its neighbors include the cube at x=2,y=2,z=3,w=3, the cube at
x=0,y=2,z=3,w=4, and so on.

The initial state of the pocket dimension still consists of a small flat region
of cubes. Furthermore, the same rules for cycle updating still apply: during
each cycle, consider the number of active neighbors of each cube.

For example, consider the same initial state as in the example above. Even
though the pocket dimension is 4-dimensional, this initial state represents a
small 2-dimensional slice of it. (In particular, this initial state defines a
3x3x1x1 region of the 4-dimensional space.)

Simulating a few cycles from this initial state produces the following
configurations, where the result of each cycle is shown layer-by-layer at each
given z and w coordinate:

Before any cycles:

z=0, w=0
.#.
..#
###


After 1 cycle:

z=-1, w=-1
# ..
..#
.#.

z=0, w=-1
# ..
..#
.#.

z=1, w=-1
# ..
..#
.#.

z=-1, w=0
# ..
..#
.#.

z=0, w=0
#.#
.##
.#.

z=1, w=0
# ..
..#
.#.

z=-1, w=1
# ..
..#
.#.

z=0, w=1
# ..
..#
.#.

z=1, w=1
# ..
..#
.#.


After 2 cycles:

z=-2, w=-2
.....
.....
..#..
.....
.....

z=-1, w=-2
.....
.....
.....
.....
.....

z=0, w=-2
# ..
##.##
#...#
.#..#
.###.

z=1, w=-2
.....
.....
.....
.....
.....

z=2, w=-2
.....
.....
..#..
.....
.....

z=-2, w=-1
.....
.....
.....
.....
.....

z=-1, w=-1
.....
.....
.....
.....
.....

z=0, w=-1
.....
.....
.....
.....
.....

z=1, w=-1
.....
.....
.....
.....
.....

z=2, w=-1
.....
.....
.....
.....
.....

z=-2, w=0
# ..
##.##
#...#
.#..#
.###.

z=-1, w=0
.....
.....
.....
.....
.....

z=0, w=0
.....
.....
.....
.....
.....

z=1, w=0
.....
.....
.....
.....
.....

z=2, w=0
# ..
##.##
#...#
.#..#
.###.

z=-2, w=1
.....
.....
.....
.....
.....

z=-1, w=1
.....
.....
.....
.....
.....

z=0, w=1
.....
.....
.....
.....
.....

z=1, w=1
.....
.....
.....
.....
.....

z=2, w=1
.....
.....
.....
.....
.....

z=-2, w=2
.....
.....
..#..
.....
.....

z=-1, w=2
.....
.....
.....
.....
.....

z=0, w=2
# ..
##.##
#...#
.#..#
.###.

z=1, w=2
.....
.....
.....
.....
.....

z=2, w=2
.....
.....
..#..
.....
.....

After the full six-cycle boot process completes, 848 cubes are left in the
active state.

Starting with your given initial configuration, simulate six cycles in a
4-dimensional space. How many cubes are left in the active state after the sixth
cycle?

"""

import os.path
import unittest
from functools import lru_cache
from typing import Iterable, Set, Tuple

Cube = Tuple[int, int, int, int]
Universe = Set[Cube]

NEIGHBOR_DISTANCE = 1


def solve() -> None:
    lines = load_input_file()
    universe = parse_lines(lines)
    for _ in range(6):
        universe = iterate(universe)
    return len(universe)


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def parse_lines(lines: Iterable[str], x=0, y=0, z=0, w=0) -> Universe:
    return set(
        (xx, yy, z, w)
        for xx, row in enumerate(lines, x)
        for yy, col in enumerate(row, y)
        if col == '#'
    )


def build_lines(universe: Universe) -> Iterable[str]:

    (
        (min_x, min_y, min_z, min_w),
        (max_x, max_y, max_z, max_w),
    ) = find_limits(universe)

    result = []
    for w in range(min_w, max_w + 1):
        ww = []
        for z in range(min_z, max_z + 1):
            zz = []
            for x in range(min_x, max_x + 1):
                zz.append(''.join((
                    '#' if (x, y, z, w) in universe else '.'
                    for y in range(min_y, max_y + 1)
                )))
            ww.append(tuple(zz))
        result.append(tuple(ww))
    return result


def iterate(universe: Universe) -> Universe:
    result = set()
    for cube in get_all_cubes(universe):
        active = cube in universe
        num_active_neighbors = len(get_neighbors(*cube) & universe)
        if active:
            if 2 <= num_active_neighbors <= 3:
                result.add(cube)
        else:
            if num_active_neighbors == 3:
                result.add(cube)
    return result


def get_all_cubes(universe: Universe) -> Universe:

    (
        (min_x, min_y, min_z, min_w),
        (max_x, max_y, max_z, max_w),
    ) = find_limits(universe)

    return (
        (x, y, z, w)
        for x in range(min_x - NEIGHBOR_DISTANCE, max_x + NEIGHBOR_DISTANCE + 1)
        for y in range(min_y - NEIGHBOR_DISTANCE, max_y + NEIGHBOR_DISTANCE + 1)
        for z in range(min_z - NEIGHBOR_DISTANCE, max_z + NEIGHBOR_DISTANCE + 1)
        for w in range(min_w - NEIGHBOR_DISTANCE, max_w + NEIGHBOR_DISTANCE + 1)
    )


def get_neighbors(x: int, y: int, z: int, w: int) -> Universe:
    return set(
        (xx, yy, zz, ww)
        for xx in (x - NEIGHBOR_DISTANCE, x, x + NEIGHBOR_DISTANCE)
        for yy in (y - NEIGHBOR_DISTANCE, y, y + NEIGHBOR_DISTANCE)
        for zz in (z - NEIGHBOR_DISTANCE, z, z + NEIGHBOR_DISTANCE)
        for ww in (w - NEIGHBOR_DISTANCE, w, w + NEIGHBOR_DISTANCE)
        if not (xx == x and yy == y and zz == z and ww == w)
    )


def find_limits(universe: Universe) -> Tuple[Cube, Cube]:
    return (
        (
            min(x for x, _, _, _ in universe),
            min(y for _, y, _, _ in universe),
            min(z for _, _, z, _ in universe),
            min(w for _, _, _, w in universe),
        ),
        (
            max(x for x, _, _, _ in universe),
            max(y for _, y, _, _ in universe),
            max(z for _, _, z, _ in universe),
            max(w for _, _, _, w in universe),
        ),
    )


class Tests(unittest.TestCase):

    lines = (
        '.#.',
        '..#',
        '###',
    )

    universe = (
        (0, 1, 0, 0),
        (1, 2, 0, 0),
        (2, 0, 0, 0),
        (2, 1, 0, 0),
        (2, 2, 0, 0),
    )

    def test_parse_lines(self):
        result = parse_lines(self.lines)
        self.assertSequenceEqual(sorted(result), sorted(self.universe))

    def test_build_lines(self):
        universe = parse_lines(self.lines)
        result = build_lines(universe)
        self.assertSequenceEqual(result, [(self.lines,)])

    def test_iterate_result(self):
        universe = set(self.universe)
        for _ in range(6):
            universe = iterate(universe)
        self.assertEqual(len(universe), 848)

    def test_get_neighbors(self):
        cube = (0, 0, 0, 0)
        result = tuple(get_neighbors(*cube))
        self.assertEqual(len(result), 80)
        expected = tuple(
            (x, y, z, w)
            for x in (-1, 0, 1)
            for y in (-1, 0, 1)
            for z in (-1, 0, 1)
            for w in (-1, 0, 1)
            if (x, y, z, w) != (0, 0, 0, 0)
        )
        self.assertSequenceEqual(sorted(result), sorted(expected))


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    print(solve())
