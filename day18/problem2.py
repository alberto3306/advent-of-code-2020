#!/usr/bin/env python3

"""
--- Part Two ---

You manage to answer the child's questions and they finish part 1 of their
homework, but get stuck when they reach the next section: advanced math.

Now, addition and multiplication have different precedence levels, but they're
not the ones you're familiar with. Instead, addition is evaluated before
multiplication.

For example, the steps to evaluate the expression 1 + 2 * 3 + 4 * 5 + 6 are now
as follows:

1 + 2 * 3 + 4 * 5 + 6
  3   * 3 + 4 * 5 + 6
  3   *   7   * 5 + 6
  3   *   7   *  11
     21       *  11
         231

Here are the other examples from above:

    1 + (2 * 3) + (4 * (5 + 6)) still becomes 51.
    2 * 3 + (4 * 5) becomes 46.
    5 + (8 * 3 + 9 + 3 * 4 * 3) becomes 1445.
    5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4)) becomes 669060.
    ((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2 becomes 23340.

What do you get if you add up the results of evaluating the homework problems
using these new rules?

"""

import os.path
import re
import unittest
from typing import Iterable


def solve() -> int:
    lines = load_input_file()
    results = map(calculate_formula, lines)
    return sum(results)


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def calculate_formula(formula: str) -> int:
    formula = f' {formula.strip()} '
    while True:
        prev = str(formula)
        if '(' in formula:
            groups = re.findall(r'(\(\d+( [*+] \d+)+\))', f'{formula}')
            for group, *_ in groups:
                repl = calculate_formula(group.strip('()'))
                formula = formula.replace(group, str(repl))
            formula = re.sub(r'\((\d+)\)', r'\1', formula)
        else:
            return calculate_simple(formula)
        assert formula != prev


def calculate_simple(formula: str) -> int:
    formula = f' {formula.strip()} '
    for op in '+*':
        while True:
            groups = re.findall(r' \d+ [%s] \d+ ' % op, formula)
            for group in groups:
                repl = str(eval(group))  # pylint: disable=eval-used
                formula = formula.replace(group, f' {repl} ')
            if not groups:
                break
    return int(formula)


class Tests(unittest.TestCase):

    def test_calculate_formula(self):

        cases = (
            ('1 + 2 * 3 + 4 * 5 + 6', 231),
            ('1 + (2 * 3) + (4 * (5 + 6))', 51),
            ('2 * 3 + (4 * 5)', 46),
            ('5 + (8 * 3 + 9 + 3 * 4 * 3)', 1445),
            ('5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))', 669060),
            ('((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2', 23340),
            ('3 * 3 * 196 * 5 * 9 * 9', 714420),
        )

        for line, expected in cases:
            result = calculate_formula(line)
            self.assertEqual(result, expected, line)


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    print(solve())
