#!/usr/bin/env python3

"""
--- Part Two ---

As you look over the list of messages, you realize your matching rules aren't
quite right. To fix them, completely replace rules 8: 42 and 11: 42 31 with the
following:

8: 42 | 42 8
11: 42 31 | 42 11 31

This small change has a big impact: now, the rules do contain loops, and the
list of messages they could hypothetically match is infinite. You'll need to
determine how these changes affect which messages are valid.

Fortunately, many of the rules are unaffected by this change; it might help to
start by looking at which rules always match the same set of values and how
those rules (especially rules 42 and 31) are used by the new versions of rules 8
and 11.

(Remember, you only need to handle the rules you have; building a solution that
could handle any hypothetical combination of rules would be significantly more
difficult.)

For example:

42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: "a"
11: 42 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: "b"
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1

abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
bbabbbbaabaabba
babbbbaabbbbbabbbbbbaabaaabaaa
aaabbbbbbaaaabaababaabababbabaaabbababababaaa
bbbbbbbaaaabbbbaaabbabaaa
bbbababbbbaaaaaaaabbababaaababaabab
ababaaaaaabaaab
ababaaaaabbbaba
baabbaaaabbaaaababbaababb
abbbbabbbbaaaababbbbbbaaaababb
aaaaabbaabaaaaababaa
aaaabbaaaabbaaa
aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
babaaabbbaaabaababbaabababaaab
aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba

Without updating rules 8 and 11, these rules only match three messages:
bbabbbbaabaabba, ababaaaaaabaaab, and ababaaaaabbbaba.

However, after updating rules 8 and 11, a total of 12 messages match:

    bbabbbbaabaabba
    babbbbaabbbbbabbbbbbaabaaabaaa
    aaabbbbbbaaaabaababaabababbabaaabbababababaaa
    bbbbbbbaaaabbbbaaabbabaaa
    bbbababbbbaaaaaaaabbababaaababaabab
    ababaaaaaabaaab
    ababaaaaabbbaba
    baabbaaaabbaaaababbaababb
    abbbbabbbbaaaababbbbbbaaaababb
    aaaaabbaabaaaaababaa
    aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
    aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba

After updating rules 8 and 11, how many messages completely match rule 0?

"""

from collections import defaultdict
import re
import os.path
import unittest
from typing import Iterable, Sequence

Rules = Sequence[Iterable[str]]

PATCHED_RULES = (
    '8: 42 | 42 8',
    '11: 42 31 | 42 11 31',
)


def solve() -> None:
    lines = load_input_file()
    lines = patch_lines(lines)
    rules = parse_rules(lines)
    messages = parse_messages(lines)
    return sum(
        match_message(message, rules)
        for message in messages
    )


def load_input_file() -> Iterable[str]:
    input_file = os.path.join(os.path.dirname(__file__), 'input.txt')
    with open(input_file) as fd:
        return map(str.strip, fd.readlines())


def patch_lines(lines: Iterable[str]) -> Iterable[str]:

    lines_to_patch = tuple(
        (line.partition(': ')[0], line)
        for line in PATCHED_RULES
    )

    for line in lines:
        num, _, _ = line.partition(': ')
        for patch_num, patch_line in lines_to_patch:
            if num == patch_num:
                yield patch_line
                break
        else:
            yield line


def parse_rules(lines: Iterable[str]) -> Rules:

    rules = defaultdict(list)

    for line in lines:
        if not line:
            break
        num, _, line = line.partition(': ')
        num = int(num)
        literal_match = re.match(r'"(\w)"', line)
        if literal_match:
            rules[num] = literal_match.groups()
        else:
            while line:
                opt, _, line = line.partition(' | ')
                rules[num].append(tuple(map(int, opt.split())))

    pattern = do_rule(rules)
    pattern = f'^{pattern}$'
    return re.compile(pattern)


def parse_messages(lines: Iterable[str]) -> Iterable[str]:
    yield from lines


def match_message(message: str, rules: Rules):
    match = rules.match(message)
    return bool(match)


def do_rule(rules, num=0):

    rule = rules[num]
    rule = fix_recursion(rule, num)
    rule = fix_balanced_pair(rule, num)

    result = '|'.join(
        ''.join(do_combination(rules, combination))
        for combination in rule
    )

    if len(result) > 1:
        result = f'({result})'

    return result


def fix_recursion(rule, num):
    try:
        (a,), (b, c) = rule
    except ValueError:
        pass
    else:
        if a == b and c == num:
            return ((a, '+'),)
    return rule


def fix_balanced_pair(rule, num):
    try:
        ((a, b), (c, d, e)) = rule
    except ValueError:
        pass
    else:
        if a == c and b == e and d == num:
            # I'm not proud of this
            return ((a, b),) + tuple(
                (a, f'{{{rep}}}', b, f'{{{rep}}}')
                for rep in range(2, 5)
            )
    return rule


LITERAL_EXPRS = (r'a', r'b', r'\+', r'{\d+}')


def do_combination(rules, combination):
    for item in combination:
        try:
            any(re.match(expr, item) for expr in LITERAL_EXPRS)
        except TypeError:
            yield do_rule(rules, item)
        else:
            yield item


class Tests(unittest.TestCase):

    def test_unpatched(self):

        lines = (
            '42: 9 14 | 10 1',
            '9: 14 27 | 1 26',
            '10: 23 14 | 28 1',
            '1: "a"',
            '11: 42 31',
            '5: 1 14 | 15 1',
            '19: 14 1 | 14 14',
            '12: 24 14 | 19 1',
            '16: 15 1 | 14 14',
            '31: 14 17 | 1 13',
            '6: 14 14 | 1 14',
            '2: 1 24 | 14 4',
            '0: 8 11',
            '13: 14 3 | 1 12',
            '15: 1 | 14',
            '17: 14 2 | 1 7',
            '23: 25 1 | 22 14',
            '28: 16 1',
            '4: 1 1',
            '20: 14 14 | 1 15',
            '3: 5 14 | 16 1',
            '27: 1 6 | 14 18',
            '14: "b"',
            '21: 14 1 | 1 14',
            '25: 1 1 | 1 14',
            '22: 14 14',
            '8: 42',
            '26: 14 22 | 1 20',
            '18: 15 15',
            '7: 14 5 | 1 21',
            '24: 14 1',
        )

        cases = (
            ('abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa', False),
            ('bbabbbbaabaabba', True),
            ('babbbbaabbbbbabbbbbbaabaaabaaa', False),
            ('aaabbbbbbaaaabaababaabababbabaaabbababababaaa', False),
            ('bbbbbbbaaaabbbbaaabbabaaa', False),
            ('bbbababbbbaaaaaaaabbababaaababaabab', False),
            ('ababaaaaaabaaab', True),
            ('ababaaaaabbbaba', True),
            ('baabbaaaabbaaaababbaababb', False),
            ('abbbbabbbbaaaababbbbbbaaaababb', False),
            ('aaaaabbaabaaaaababaa', False),
            ('aaaabbaaaabbaaa', False),
            ('aaaabbaabbaaaaaaabbbabbbaaabbaabaaa', False),
            ('babaaabbbaaabaababbaabababaaab', False),
            ('aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba', False),
        )

        rules = parse_rules(lines)

        for message, expected in cases:
            result = match_message(message, rules)
            self.assertEqual(result, expected, message)

    def test_patched(self):

        lines = (
            '42: 9 14 | 10 1',
            '9: 14 27 | 1 26',
            '10: 23 14 | 28 1',
            '1: "a"',
            '11: 42 31',
            '5: 1 14 | 15 1',
            '19: 14 1 | 14 14',
            '12: 24 14 | 19 1',
            '16: 15 1 | 14 14',
            '31: 14 17 | 1 13',
            '6: 14 14 | 1 14',
            '2: 1 24 | 14 4',
            '0: 8 11',
            '13: 14 3 | 1 12',
            '15: 1 | 14',
            '17: 14 2 | 1 7',
            '23: 25 1 | 22 14',
            '28: 16 1',
            '4: 1 1',
            '20: 14 14 | 1 15',
            '3: 5 14 | 16 1',
            '27: 1 6 | 14 18',
            '14: "b"',
            '21: 14 1 | 1 14',
            '25: 1 1 | 1 14',
            '22: 14 14',
            '8: 42',
            '26: 14 22 | 1 20',
            '18: 15 15',
            '7: 14 5 | 1 21',
            '24: 14 1',
        )

        cases = (
            ('abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa', False),
            ('bbabbbbaabaabba', True),
            ('babbbbaabbbbbabbbbbbaabaaabaaa', True),
            ('aaabbbbbbaaaabaababaabababbabaaabbababababaaa', True),
            ('bbbbbbbaaaabbbbaaabbabaaa', True),
            ('bbbababbbbaaaaaaaabbababaaababaabab', True),
            ('ababaaaaaabaaab', True),
            ('ababaaaaabbbaba', True),
            ('baabbaaaabbaaaababbaababb', True),
            ('abbbbabbbbaaaababbbbbbaaaababb', True),
            ('aaaaabbaabaaaaababaa', True),
            ('aaaabbaaaabbaaa', False),
            ('aaaabbaabbaaaaaaabbbabbbaaabbaabaaa', True),
            ('babaaabbbaaabaababbaabababaaab', False),
            ('aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba', True),
        )

        patched_lines = patch_lines(lines)
        rules = parse_rules(patched_lines)

        failed = []
        for message, expected in cases:
            result = match_message(message, rules)
            if result != expected:
                failed.append(message)
        self.assertSequenceEqual(failed, [])

    def test_match(self):

        lines = (
            '0: 4 1 5',
            '1: 2 3 | 3 2',
            '2: 4 4 | 5 5',
            '3: 4 5 | 5 4',
            '4: "a"',
            '5: "b"',
        )

        cases = (
            ('ababbb', True),
            ('bababa', False),
            ('abbbab', True),
            ('aaabbb', False),
            ('aaaabbb', False),
        )

        rules = parse_rules(lines)

        for message, expected in cases:
            result = match_message(message, rules)
            self.assertEqual(bool(result), expected, message)


if __name__ == '__main__':
    print('Running tests...')
    print()
    unittest.main(verbosity=3, exit=False)
    print()
    print('Solving problem...')
    print(solve())
